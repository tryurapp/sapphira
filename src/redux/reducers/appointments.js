import * as types from '../types';

const initialState = {
    pickerCenter: {},
    pickerCategory: {},
    pickerService: {},
    pickerTherapist: {},
    dateSelected: null,
    timeSelected: null,
    upcomingAppointments: [],
    pastAppointments: [],
    appointment: {},
    lastAppointment: null,
    disabledDates: [],
    slots: []
};


export default function loginReducer(state = initialState, action={})  {
    switch(action.type) {
        case types.UPDATE_CENTER:
            return {
                ...state,
                pickerCenter: action.pickerCenter
        }
        case types.UPDATE_CATEGORY:
            return {
                ...state,
                pickerCategory: action.pickerCategory
        }
        case types.UPDATE_SERVICE:
            return {
                ...state,
                pickerService: action.pickerService
        }
        case types.UPDATE_THERAPIST:
            return {
                ...state,
                pickerTherapist: action.pickerTherapist
        }
        case types.UPDATE_DATE_SELECTED:
            return {
                ...state,
                dateSelected: action.dateSelected
        }
        case types.UPDATE_TIME_SELECTED:
            return {
                ...state,
                timeSelected: action.timeSelected
        }
        case types.UPDATE_UPCOMING_APPOINTMENTS:
            return {
                ...state,
                upcomingAppointments: action.upcomingAppointments
        }
        case types.UPDATE_PAST_APPOINTMENTS:
            return {
                ...state,
                pastAppointments: action.pastAppointments
        }
        case types.UPDATE_LAST_APPOINTMENT:
            return {
                ...state,
                lastAppointment: action.lastAppointment
        }
        case types.DISABLED_DATES:
            return {
                ...state,
                disabledDates: action.disabledDates
        }
        case types.SLOTS:
            return {
                ...state,
                slots: action.slots
        }
        default:
            return state;
    }
}