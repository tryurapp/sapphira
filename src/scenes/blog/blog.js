//React imports
import React, { useEffect } from 'react';
//React native imports
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
//Components imports
import Featured from './featured'
import BlogWP from './blogWP'
import * as Utils from '../../utils'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//Redux imports
import { useSelector, useDispatch } from 'react-redux'
import * as SPActions from '../../redux/actions'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
import Back from '../../resources/blog/back'
//WebView imports
import { WebView } from 'react-native-webview'



const Blog = props => {

    //Connect Redux for functional component
    const user = useSelector(
        state => state.login.userLogged
    )
    const wpPosts = useSelector(
        state => state.blog.wpPosts
    )
    const wpFeatured = useSelector(
        state => state.blog.wpFeatured
    )
    
    
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const getWPPosts = () => {
        dispatch(SPActions.blog.getWPPosts())
    }
    const getUserLoggedData = (email) => {
        dispatch(SPActions.login.getUserLoggedData(email))
    }
    const getVersion = () => {
        dispatch(SPActions.blog.getVersion())
    }

    //UseEffect
    useEffect( () => {
        getVersion()
        getWPPosts()
        if (props.email && props.email !== 'invited') {
            getUserLoggedData(props.email)
        }
    }, [])

    

    //Render component
    return(
        <View style={ styles.container }>
            {
                (user.email === 'invited') 
                ?
                <TouchableOpacity 
                    style={ styles.imageBack }
                    onPress={ () => {
                        Actions.replace("WelcomeScreen") }
                    }
                >
                    <Back />
                </TouchableOpacity>
                :
                <View style={{ height: hp('7') }} />
            }
            <View style={{ height: hp('0') }} />
            <BlogWP />
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9',
    },
    imageBack: {//
        marginLeft: wp('6'),
        marginTop: hp('5'),
        //position: 'absolute',
        //top: hp('0'),
        //left: wp('0'),
    },
    titleStyle: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.75'),
        marginLeft: wp('7.46'),
        paddingLeft: wp('2.5'),
    },
    image: {
        marginTop: hp('4.75'),
        marginLeft: wp('9.36'),
    },
})

export default Blog