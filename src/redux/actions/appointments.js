import * as types from '../types'
//Firebase imports
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/firestore'
import moment from 'moment'
import * as Utils from '../../utils'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'


export const updatePickerCenter = (pickerCenter) => {
    return {
        type: types.UPDATE_CENTER,
        pickerCenter
    }
}

export const updatePickerCategory = (pickerCategory) => {
    return {
        type: types.UPDATE_CATEGORY,
        pickerCategory
    }
}

export const updatePickerService = (pickerService) => {
    return {
        type: types.UPDATE_SERVICE,
        pickerService
    }
}

export const updatePickerTherapist = (pickerTherapist) => {
    return {
        type: types.UPDATE_THERAPIST,
        pickerTherapist
    }
}

export const updateDateSelected = (dateSelected) => {
    return {
        type: types.UPDATE_DATE_SELECTED,
        dateSelected
    }
}

export const updateTimeSelected = (timeSelected) => {
    return {
        type: types.UPDATE_TIME_SELECTED,
        timeSelected
    }
}

export const updateUpcomingAppointments = (upcomingAppointments) => {
    return {
        type: types.UPDATE_UPCOMING_APPOINTMENTS,
        upcomingAppointments
    }
}

export const updatePastAppointments = (pastAppointments) => {
    return {
        type: types.UPDATE_PAST_APPOINTMENTS,
        pastAppointments
    }
}

export const updateLastAppointment = (lastAppointment) => {
    return {
        type: types.UPDATE_LAST_APPOINTMENT,
        lastAppointment
    }
}

//Update Appointment
export function updateAppointment(appointment) {
    return (dispatch, getState) => {
        const state = getState()

        firebase.firestore().collection('Slots')
        .where('serviceDate', '==', appointment.serviceDate)
        .get()
        .then( query => {
            if (query) {
                const users = []
                query.forEach( doc => {
                    users.push(doc.data())
                })
                if (users[0]) {
                    Actions.SPAlert({ 
                        title: `Atención.`, 
                        text: 'La cita ha sido asignada en este momento. Seleccione otra hora para la misma.',
                        setHeight: hp('4')
                    })
                } else {
                    firebase.firestore().collection('Slots')
                    .add(appointment)
                    .then( refDoc => {
                        dispatch(updatePickerCenter({}))
                        dispatch(updatePickerCategory({}))
                        dispatch(updatePickerService({}))
                        dispatch(updatePickerTherapist({}))
                        dispatch(updateDateSelected(''))
                        dispatch(updateTimeSelected(''))
                        //Convert serviceDate to center zone
                        const zone = appointment.centerZone ? appointment.centerZone  : 'America/New_York'

                        const dateToConvert = Utils.functions.convertZoneTimeToLocalTime(appointment.serviceDate.toDate(), zone)

                        const dateAppointment = firebase.firestore.Timestamp.fromDate(dateToConvert)
                        appointment.serviceDate = dateAppointment

                        Actions.replace('SavedAppointment', { appointment: appointment, type: 'sendEmail' })
                    })
                    .catch( error => {
                        console.log('ErrorUpdatingAppointment: ', error.message)
                    })
                }
            }
        })
        .catch( error => {
            console.log('Error al acceder a las citas.', error.message)
        })
    }
}

//Get Centers info
export function getCentersData() {
    return (dispatch, getState) => {
        const state = getState()
        firebase.firestore().collection('CompleteCenters')
        .onSnapshot( query => {
            if (query) {
                const centers = []
                query.forEach( doc => {
                    centers.push(doc.data())
                })
                dispatch(updateCentersData(centers))
            }
        })
    }
}

export const updateCentersData = (centers) => {
    return {
        type: types.BLOG_UPDATE_CENTERS,
        centers
    }
}

//Get Slots
export function getSlots(fromDate, toDate) {
    return (dispatch, getState) => {
        const state = getState()
        //Select disabled days in calendar
        const disabledDates = []
        for (var i=1; i<63; i++) {
            const date = moment(new Date).add(i, 'days')
            const day = date.format('dd')
            if ( day === 'Su' ) {
                disabledDates.push(date)
            }
        }
        dispatch(updateDisabledDates(disabledDates))
    
        let dateFrom
        let dateTo
        if (fromDate) {
            dateFrom = fromDate
        } else {
            let dateToConvert = new Date(moment(new Date).subtract(Utils.constants.MAX_TIME_FOR_APPOINTMENT_IN_MONTHS, 'months').calendar())
            dateFrom = firebase.firestore.Timestamp.fromDate(dateToConvert)
        }
        if(toDate) {
            dateTo = toDate
        } else {
            let dateToConvert = new Date(moment(new Date).add(Utils.constants.MAX_TIME_FOR_APPOINTMENT_IN_MONTHS, 'months').calendar())
            dateTo = firebase.firestore.Timestamp.fromDate(dateToConvert)
        }

        firebase.firestore().collection('Slots')
        .where('serviceDate', '>=', dateFrom)
        .where('serviceDate', '<=', dateTo)
        .orderBy('serviceDate')
        .onSnapshot( query => {
            if (query) {
                let slots = []
                query.forEach( doc => {
                    let slotToPrepare = doc._data

                    const zone = slotToPrepare.centerZone ? slotToPrepare.centerZone  : 'America/New_York'

                    const dateToConvert = Utils.functions.convertZoneTimeToLocalTime(slotToPrepare.serviceDate.toDate(), zone)
                    const dateAppointment = firebase.firestore.Timestamp.fromDate(dateToConvert)

                    slotToPrepare.serviceDate = dateAppointment

                    slots.push(slotToPrepare)
                })

                dispatch(updateSlots(slots))
                dispatch(selectAppointments(slots))
            }
        })
    }
}

function selectAppointments(slots) {
    return (dispatch, getState) => {
        const state = getState()
        //Convert actual date to centerZone date
        const timestampNow = firebase.firestore.Timestamp.fromDate(new Date())
        const newUpcomingAppointments = []
        const newPastAppointments = []
        slots.forEach( item => {
            if(item.customerEmail === state.login.userLogged.email) {
                if (item.serviceDate > timestampNow) {
                    newUpcomingAppointments.push(item)
                } else {
                    newPastAppointments.push(item)
                }
                newPastAppointments.sort( (a, b) => {
                    return b.serviceDate - a.serviceDate
                })
            }
        })
        //console.log('Proximos: ', newUpcomingAppointments)
        //console.log('Pasados: ', newPastAppointments)
        setTimeout( () => {
            dispatch(updateUpcomingAppointments(newUpcomingAppointments))
        }, 1000)
        dispatch(updatePastAppointments(newPastAppointments))
        dispatch(updateLastAppointment(newPastAppointments[0]))
    }
}

export const updateDisabledDates = (disabledDates) => {
    return {
        type: types.DISABLED_DATES,
        disabledDates
    }
}

export const updateSlots = (slots) => {
    return {
        type: types.SLOTS,
        slots
    }
}
