//React imports
import React, { useState, useEffect } from 'react';
//React native imports
import { View, StyleSheet, Text, FlatList, TouchableOpacity, Image } from 'react-native';
//Components imports
import BackSaved from '../../resources/profile/backSaved'
import FavouriteBlog from '../../resources/blog/favouriteBlog'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//Redux imports
import { useSelector } from 'react-redux'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
//Moments imports
import moment from 'moment'



const Favourites = props => {

    //Connect Redux for functional component
    const wpPosts = useSelector(
        state => state.blog.wpPosts
    )
    const user = useSelector(
        state => state.login.userLogged
    )

    //State
    const [favourites, setFavourites] = useState([])

    //UseEffect
    useEffect( () => {
        let fav = []
        // Get user favourites
        const userFav = user.favourites
        wpPosts.forEach( post => {
            userFav.forEach( favourite => {
                if (favourite === post.id) {
                    fav.push(post)
                }
            })
        })
        //Select favourites
        setFavourites(fav)
    }, [])

    //Renders
    const setTitlePost = (text, date, itemId) => {
        let textCapitalized = ''
        if (text[0] !== '¿' && text[0] !== '!') {
            textCapitalized = text[0].toUpperCase() + text.slice(1)
        } else {
            textCapitalized = text[0] + text[1].toUpperCase() + text.slice(2)
        }
        moment.locale()
        const newDate = moment(date).format("ll")
        return(
            <View style={ styles.styleViewPost }>
                <View style={{ flexDirection: 'column' }}>
                    <Text style={ styles.textStyle }>{ textCapitalized } ...</Text>
                    <Text style={ styles.subtitle }>{ newDate }</Text>
                </View>
                {
                    renderFav(itemId)
                }
            </View>
        ) 
    }

    const renderFav = (postId) => {
        let isFav = false
        if (user.favourites) {
            user.favourites.forEach( item => {
                if (item === postId) {
                    isFav = true
                }
            })
        }
        return (
            <View style={{ justifyContent: 'center' }}>
                {
                    isFav ? <FavouriteBlog /> : null
                }
            </View>
        )
    }

    const renderPost = (item) => {
        const postText = setTitlePost(item.title.rendered.substring(0, 40).toLowerCase(), item.date, item.id)
        return(
            <TouchableOpacity 
                onPress={ () => Actions.PostDetail({ item })}
            >
                {
                    item._embedded['wp:featuredmedia'].filter( element => element.id == item.featured_media).map( (subitem, index) => (
                        <View style={ styles.cardStyle }>
                            <Image
                                source={{ uri: subitem.media_details.sizes.medium.source_url }}
                                style={ styles.imagePost }
                                key={ item.id }
                            />
                            {  postText }
                        </View>
                    ))
                }
            </TouchableOpacity>
        )
    }

    return(
        <View>
            <View style={{ height: hp('10') }} />
            <TouchableOpacity 
                style={ styles.imageBack }
                onPress={ () => Actions.pop() }
            >
                <BackSaved />
            </TouchableOpacity>
            <Text style={ styles.titleStyle }>Guardados</Text>
            <View style={{ height: hp('2.7') }} />
            {
                favourites[0] ?
                    <FlatList
                        style={ styles.flatListStyle } 
                        data={ favourites }
                        renderItem={ ({item}) => renderPost(item) }
                        ItemSeparatorComponent={
                            Platform.OS !== 'android' &&
                                (({ highlighted }) => (
                                <View
                                    style={ styles.separator}
                                />
                                ))
                        }
                        keyExtractor={ (item, index) => `${ item.id.toString() } + ${ index.toString() }` }
                        showsVerticalScrollIndicator={ true }
                        nestedScrollEnabled={ true }
                    />
                :
                    <View>
                        <Text style={ styles.title }>No tienes seleccionado ningún post como guardado.</Text>
                    </View>
            }
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
    },
    flatListStyle: {
        marginLeft: wp('9.36'),
        //height: hp('25.5')
    },
    cardStyle: {
        height: hp('19.22'),
        width: wp('81.34'),
        borderRadius: hp('1.62'),
        shadowOffset: { width: wp('1'), height: wp('1') },
    },
    styleViewPost: {
        width: wp('81.34'),
        borderBottomLeftRadius: hp('1.62'),
        borderBottomRightRadius: hp('1.62'),
        flex: 1,
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: '#1A1A1A50',
        height: hp('4.12'),
        bottom: hp('0'),
        left: wp('0'),
    },
    separator: {
        height: hp('4.86'),
    },
    imagePost: {
        flex: 1,
        width: wp('81.34'),
        height: hp('19.22'),
        position: 'relative',
        resizeMode: 'cover',
        borderRadius: hp('1.62'),
    },
    textStyle: {//
        color: '#FFFFFF',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        marginLeft: wp('3.48'),
        marginTop: hp('0.5'),
        width:wp('70')
    },
    subtitle: {//
        color: '#FFFFFF',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.3'),
        marginLeft: wp('3.48'),
    },
    titleStyle: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.75'),
        marginLeft: wp('13'),
        paddingLeft: wp('2.5'),
    },
    imageBack: {
        marginLeft: wp('6'),
        marginTop: hp('5'),
        position: 'absolute',
        top: hp('0'),
        left: wp('0'),
    },
    title: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
        color: '#222427',
        marginLeft: wp('10'),
        marginTop: hp('1'),
    },
})

export default Favourites