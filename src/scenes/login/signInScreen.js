//React imports
import React, { useState, useEffect } from 'react'
//React native imports
import { Text, StyleSheet, View, Image, TouchableOpacity, Dimensions, Platform } from 'react-native'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
//Components imports
import TUA_Button from '../../components/TUA_Button'
import SapphiraInput from '../../components/SapphiraInput'
//Validate imports
import validator from 'email-validator'
//Keyboard imports
import { KeyboardAvoidingScrollView } from 'react-native-keyboard-avoiding-scroll-view'
//Firebase imports
import firebase from '@react-native-firebase/app'
import auth from '@react-native-firebase/auth'
//Redux imports
import { useSelector, useDispatch } from 'react-redux'
import * as SPActions from '../../redux/actions'
//Images imports
import RememberOn from '../../resources/login/rememberOn'
import RememberOff from '../../resources/login/rememberOff'
import Back from '../../resources/appointments/backDateHours'
//Utils imports
import * as Utils from '../../utils'
import { getVersion } from '../../redux/actions/blog'


const SignInScreen = (props) => {

    //Connect Redux for functional component
    const userToRemember = useSelector(
        state => state.login.userToRemember
    )
    const version = useSelector(
        state => state.blog.version
    )

    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updateUserLogged = (userLogged) => {
        dispatch(SPActions.login.updateUserLogged(userLogged))
    }
    const updateUserToRemember = (userToRemember) => {
        dispatch(SPActions.login.updateUserToRemember(userToRemember))
    }
    const getUserLoggedData = (email) => {
        dispatch(SPActions.login.getUserLoggedData(email))
    }
    const getVersion = () => {
        dispatch(SPActions.blog.getVersion())
    }

    //State
    const [email, setEmail] = useState(props.email ? props.email : '')
    const [emailError, setEmailError] = useState('')
    const [password, setPassword] = useState(props.password ? props.password : '')
    const [passwordError, setPasswordError] = useState('')
    const [warning, setWarning] = useState('')
    const [activity, setActivity] = useState(false)
    const [toRemember, setToRemember] =useState(userToRemember ? true : false)

    //UseEffect
    useEffect( () => {
        getVersion()
        if (userToRemember) {
            setEmail(userToRemember.email)
            setPassword(userToRemember.password)
        }
    }, [userToRemember])
    
    //Functions
    const validateForm = () => {
        let valid = true
        let errors = {}
        //Validation of email
        if (!email)  {
            errors.email = 'Introduce un email'
            valid = false
        } else if (!validator.validate(email)) {
            errors.email = 'Introduce un email válido'
            valid = false
        }
        //Validation of password
        if (!password)  {
            errors.password = 'Introduce una contraseña'
            valid = false
        } else if (password.length < 6){
            setWarning('La contraseña debe tener al menos 6 caracteres')
            valid = false
        } else {
            setWarning('')
        }
        //Update errors for render
        if (errors.email) {
            setEmailError(errors.email ? errors.email : '')
            valid = false
        } else {
            setEmailError('')
        }
        if (errors.password) {
            setPasswordError(errors.password ? errors.password : '')
            valid = false
        } else {
            setPasswordError('')
        }
        //Return validation
        return valid
    }

    const validateEmail = () => {
        let valid = true
        let errors = {}
        //Validation of email
        if (!email)  {
            errors.email = 'Introduce un email'
            valid = false
        } else if (!validator.validate(email)) {
            errors.email = 'Introduce un email válido'
            valid = false
        }
        //Update errors for render
        if (errors.email) {
            setEmailError(errors.email ? errors.email : '')
            valid = false
        } else {
            setEmailError('')
        }
        //Return validation
        return valid
    }

    const register = () => {
        Actions.replace("SignUpScreen")
    }

    const userNotFound = () => {
        Actions.SPAlert({ 
            title: `${ email }, no estas registrado.`, 
            text: 'Pulsa en Regístrate para hacerlo.',
            setHeight: hp('4')
        })
    }

    const login = () => {
        if (!validateForm()) return null
        setActivity(true)
        auth()
        .signInWithEmailAndPassword(email, password)
        .then( () => {
            loginUser()
        })
        .catch( error => {
            setActivity(false)
            if (error.code === "auth/user-not-found") {
                userNotFound()
            } else if (error.code === "auth/wrong-password") {
                Actions.SPAlert({ 
                    title: `${ email }, has introducido tu contraseña mal.`, 
                    text: 'Vuelve a introducirla o pulsa en "Olvidé mi contraseña" para restablecerla.',
                    setHeight: hp('4')
                })
            } else {
                Actions.SPAlert({ 
                    title: 'Error', 
                    text: `${ error.message }`,
                    setHeight: hp('4')
                })
            }
        })
    }

    const loginUser = () => {
        const userToRemember = {
            email: email,
            password: password,
        }
        setEmail('')
        setEmailError('')
        setPassword('')
        setPasswordError('')
        setWarning('')
        setActivity(false)
        getUserLoggedData(email)
        toRemember ? updateUserToRemember(userToRemember) : updateUserToRemember({})

        const vers = Platform.OS === 'ios' ? Utils.constants.IOS_VERSION : Utils.constants.ANDROID_VERSION

        if (version !== vers) {
            Actions.SPAlert({ 
                title: `Nueva versión disponible`, 
                text: `Hay una nueva versión disponible de la aplicación. Acceda a la store a descargarla`,
                setHeight: hp('4'),
                onPress: 'GoToStore' ,
                buttonText: Platform.OS === 'android' ? 'Ir a la Store' : 'Continuar',
                email: email,
            })
        } else {
            Actions.replace("_Blog", { email })
        }
    }

    const restorePassword = () => {
        if (!validateEmail()) return null
        console.log('Email: ', email)
        firebase.auth().sendPasswordResetEmail(email)
        .then( () => {
            Actions.SPAlert({ 
                title: `${ email }`, 
                text: `Se le ha enviado un email a ${ email } con instrucciones para resetear su password`,
                setHeight: hp('4')
            })
        })
        .catch( (e) => {
            Actions.SPAlert({ 
                title: `${ email }`, 
                text: `Email no registrado en nuestra base de datos`,
                setHeight: hp('4')
            })
        })
    }

    const changeRemember = () => {
        setToRemember(!toRemember)
    }

    //Renders
    return(
        <View style={ styles.textStyle }>
            <KeyboardAvoidingScrollView
                containerStyle={ styles.formStyle }
            >
                <View style={{ height: hp('5') }} />
                <TouchableOpacity 
                    style={ styles.imageBack }
                    onPress={ () => Actions.replace('WelcomeScreen') }
                >
                    <Back />
                </TouchableOpacity>
                <Image
                    source={ require('../../resources/Logo.png')}
                    resizeMode='contain'
                    style={ styles.image }
                />
                <View style={{ height: Platform.OS === 'ios' ? hp('20.2') : hp('12') }} />
                <Text style={ styles.text }>Inicia sesión</Text>
                <View style={{ height: hp('4.32') }} />
                <SapphiraInput
                    label='Correo electrónico'
                    placeholder='Correo electrónico'
                    value={ email }
                    error={ emailError }
                    onChangeText={ v => setEmail(v) }
                    keyboardType='email-address'
                    onSubmitEditing={ validateForm }
                    onChange={ validateEmail }
                    multiline={ false} 
                    isPassword={ false }
                />
                <View style={{ height: hp('2.8') }} />
                <SapphiraInput
                    label='Contraseña'
                    placeholder='Contraseña'
                    value={ password }
                    warning={ warning }
                    error={ passwordError }
                    onChangeText={ v => setPassword(v) }
                    keyboardType='default'
                    onSubmitEditing={ validateForm }
                    onEndEditing={ validateForm }
                    onChange={ validateForm }
                    multiline={ false} 
                    isPassword={ true }
                />
                <View style={{ height: Platform.OS === 'android' ? hp('4') : hp('1.08') }} />
                <View style={{ flexDirection: 'row' }}>
                    {
                        toRemember 
                        ? 
                            <TouchableOpacity
                                transparent
                                onPress={ changeRemember }
                            >
                                <View style={{ flexDirection: 'row', width: wp('45') }}>
                                    <RememberOn 
                                        style={{ marginLeft: wp('7.46') }}
                                    />
                                    <Text style={ [styles.textRegisterQuestion, { marginLeft: wp('2') }] }>Recordarme</Text>
                                </View>
                            </TouchableOpacity>
                        :
                            <TouchableOpacity
                                transparent
                                onPress={ changeRemember }
                            >
                                <View style={{ flexDirection: 'row', width: wp('45') }}>
                                    <RememberOff 
                                        style={{ marginLeft: wp('7.46') }}
                                    />
                                    <Text style={ [styles.textRegisterQuestion, { marginLeft: wp('2') }] }>Recordarme</Text>
                                </View>
                            </TouchableOpacity>
                    } 
                    <TouchableOpacity
                        transparent
                        onPress={ restorePassword }
                    >
                        <Text style={ styles.textRegister }>Olvidé mi contraseña</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ height: hp('6.48') }} />
                <TUA_Button
                    bgColor={ '#CBAC77' }
                    label={ 'Iniciar sesión' }
                    labelColor={ '#FFFFFF'}
                    setWidth={ Dimensions.get('window').width - wp('14.96') }
                    onPress={ login }
                    fontSize={ hp('1.7') }
                    setActivity={ activity }
                    setHeight={ hp('5.5') }
                    buttonStyle={{ borderRadius: hp('0.5'), marginLeft: wp('7.46') }}
                />
                <View style={{ height: hp('2.16') }} />
                <View style={ styles.registerView }>
                    <Text style={ styles.textRegisterQuestion }>¿No tienes cuenta?</Text>
                    <TouchableOpacity
                        transparent
                        style={ styles.google }
                        onPress={ register }
                    >
                        <Text style={ styles.textRegister }>   Regístrate</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingScrollView>
        </View>
    )
}

export default SignInScreen

const styles = StyleSheet.create({
    textStyle: {
        flex: 1,
        backgroundColor: '#F9F9F9',
    },
    formStyle: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        //alignItems: 'center',
    },
    viewStyle: {
        height: hp('44'),
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.75'),
        marginLeft: wp('7.46'),
        paddingLeft: wp('2.5'),
    },
    image: {
        marginTop: hp('4.75'),
        marginLeft: wp('8.2'),
    },
    registerView: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    textRegisterQuestion: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#707070',
    },
    textRegister: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#CBAC77',
        textDecorationLine: 'underline',
        marginRight: wp('10'),
    },
    imageBack: {//
        marginLeft: wp('6'),
        marginTop: hp('5'),
        position: 'absolute',
        top: hp('0'),
        left: wp('0'),
    },
})