import * as types from '../types';

const initialState = {
    userLogged: {},
    userToRemember: {},
    userToCreate: {}
};

export default function loginReducer(state = initialState, action={})  {
    switch(action.type) {
        case types.LOGIN_UPDATE_USER_LOGGED:
            return {
                ...state,
                userLogged: action.userLogged
        }
        case types.UPDATE_USER_TO_REMEMBER:
            return {
                ...state,
                userToRemember: action.userToRemember
        }
        case types.CREATE_USER_IN_FIREBASE:
            return {
                ...state,
                userToCreate: action.userToCreate
        }
        default:
            return state;
    }
}