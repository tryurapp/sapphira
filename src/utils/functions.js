import { DateTime, SystemZone } from 'luxon'
//Firebase imports
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/firestore'


export function calculateTime(hour) { 
    switch(hour) {
        case 8:
            return "08:00 am"
        case 9:
            return "09:00 am"
        case 10:
            return "10:00 am"
        case 11:
            return "11:00 am"
        case 12:
            return "12:00 am"
        case 13:
            return "01:00 pm"
        case 14:
            return "02:00 pm"
        case 15:
            return "03:00 pm"
        case 16:
            return "04:00 pm"
        case 17:
            return "05:00 pm"
        case 18:
            return "06:00 pm"
        case 19:
            return "07:00 pm"
        case 20:
            return "08:00 pm"
        case 21:
            return "09:00 pm"
        default:
            return ""
    }
}

export function calculateTimeNumber(hour) { 
    switch(hour) {
        case "08:00 am":
            return 8
        case "09:00 am":
            return 9
        case "10:00 am":
            return 10
        case "11:00 am":
            return 11
        case "10:00 am":
            return 12
        case "01:00 pm":
            return 13
        case "02:00 pm":
            return 14
        case "03:00 pm":
            return 15
        case "04:00 pm":
            return 16
        case "05:00 pm":
            return 17
        case "06:00 pm":
            return 18
        case "07:00 pm":
            return 19
        case "08:00 pm":
            return 20
        case "09:00 pm":
            return 21
        default:
            return 0
    }
}

export function calculateMonth(month) { 
    switch(month) {
        case '01':
            return "Enero"
        case '02':
            return "Febrero"
        case '03':
            return "Marzo"
        case '04':
            return "Abril"
        case '05':
            return "Mayo"
        case '06':
            return "Junio"
        case '07':
            return "Julio"
        case '08':
            return "Agosto"
        case '09':
            return "Septiembre"
        case '10':
            return "Ocyubre"
        case '11':
            return "Noviembre"
        case '12':
            return "Diciembre"
        default:
            return ""
    }
}

export function convertZoneTimeToLocalTime(zoneTime, zoneIdentifier) {

    let local = DateTime.local(
        zoneTime.getFullYear(),
        zoneTime.getMonth() + 1,
        zoneTime.getDate(),
        zoneTime.getHours(),
        zoneTime.getMinutes(),
        zoneTime.getSeconds(),
        zoneTime.getMilliseconds(),
    );

    let rezoned = local.setZone(zoneIdentifier)
        
    let newDate = new Date(rezoned.year, rezoned.month - 1, rezoned.day, rezoned.hour, rezoned.minute, rezoned.second, rezoned.millisecond)

    return newDate;
  };

  export function convertLocalTimeToZoneTime(localTime, zoneIdentifier) {

    let local = DateTime.fromObject(
      {
          year: localTime.getFullYear(),
          month: localTime.getMonth() + 1,
          day: localTime.getDate(),
          hour: localTime.getHours(),
          minutes: localTime.getMinutes(),
          seconds: localTime.getSeconds(),
          milliseconds: localTime.getMilliseconds()
      },
      {
          zone: zoneIdentifier
      }
    )
    localTime = new Date(local.ts)

    return localTime;
  };