//React imports
import React from 'react'
//React native imports
import { View, StyleSheet, Text, FlatList, Image, TouchableOpacity } from 'react-native'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React native router flux imorts
import { Actions } from 'react-native-router-flux'
//Images imports
import FeaturedBlack from '../../resources/blog/featuredBlack'
//Redux imports
import { useSelector } from 'react-redux'


const Featured = props => {

    //Connect Redux for functional component
    const wpFeatured = useSelector(
        state => state.blog.wpFeatured
    )

    //Renders
    const setTitlePost = (text) => {
        let textCapitalized = ''
        if (text[0] !== '¿' && text[0] !== '!') {
            textCapitalized = text[0].toUpperCase() + text.slice(1)
        } else {
            textCapitalized = text[0] + text[1].toUpperCase() + text.slice(2)
        }
        return(
            <View style={ styles.styleViewPost }>
                 <Text style={ styles.textStyle }>{ textCapitalized } ...</Text>
            </View>
           
        ) 
    }

    const renderPost = (item) => {
        const postText = setTitlePost(item.title.rendered.substring(0, 24).toLowerCase())
        return(
            <TouchableOpacity 
                onPress={ () => Actions.PostDetail({ item })}
            >
                {
                    item._embedded['wp:featuredmedia'].filter( element => element.id == item.featured_media).map( (subitem, index) => (
                        <View style={ styles.cardStyle }>
                            <Image
                                source={{ uri: subitem.media_details.sizes.medium.source_url }}
                                style={ styles.imagePost }
                                key={ item.id }
                            />
                            {  postText }
                        </View>
                    ))
                }
            </TouchableOpacity>
        )
    }
        
    return(
        <View style={ styles.viewStyle }>
            <View style={{ height: hp('4.74') }} />
            <Text style={ styles.titleStyle }>Destacados</Text>
            <View style={{ height: hp('2.7') }} />
            {
                wpFeatured[0] ?
                    <FlatList
                        style={ styles.flatListStyle } 
                        ItemSeparatorComponent={
                            Platform.OS !== 'android' &&
                                (({ highlighted }) => (
                                <View
                                    style={ styles.separator}
                                />
                                ))
                        }
                        data={ wpFeatured }
                        renderItem={ ({ item }) => renderPost(item) }
                        horizontal={ true }
                        keyExtractor={ (item, index) => `${ item.id.toString() } + ${ index.toString() }` }
                        showsHorizontalScrollIndicator={ true }
                        nestedScrollEnabled={ true }
                    />
                :
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ width: wp('9.36') }} />
                        <FeaturedBlack 
                            width={ wp('44.6') }
                            height={ hp('17')}
                            resizeMode='strech'
                        />
                        <FeaturedBlack 
                            width={ wp('44.6') }
                            height={ hp('17')} 
                        />
                    </View>
            }
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    viewStyle: {
        height: hp('31.56'),
    },
    titleStyle: {
        marginLeft: wp('5.5'),
        fontFamily: 'Helvetica Neue',
        fontSize: hp('3'),
    },
    flatListStyle: {
        marginLeft: wp('9.36'),
        height: hp('16.94'),
    },
    separator: {
        width: wp('9.36'),
    },
    cardStyle: {
        height: hp('17'),
        width: wp('44.6'),
        borderRadius: hp('1.62'),
        shadowOffset: { width: wp('1'), height: wp('1') },
        backgroundColor: 'pink',
    },
    imagePost: {
        flex: 1,
        width: wp('44.6'),
        position: 'relative',
        resizeMode: 'cover',
        justifyContent: 'center',
        borderRadius: hp('1.62'),
        height: hp('16.94'),
    },
    textStyle: {
        color: '#FFFFFF',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        marginLeft: wp('2.8'),
    },
    styleViewPost: {
        width: wp('44.6'),
        borderBottomLeftRadius: hp('1.62'),
        borderBottomRightRadius: hp('1.62'),
        flex: 1,
        flexDirection: 'column',
        position: 'absolute',
        backgroundColor: '#1A1A1A50',
        height: hp('3'),
        bottom: hp('0'),
        left: wp('0'),
        justifyContent: 'center',
    },
    titleStyle: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.75'),
        marginLeft: wp('7.46'),
        paddingLeft: wp('2.5'),
    },
})

export default Featured