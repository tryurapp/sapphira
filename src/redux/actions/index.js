import * as login from './login'
import * as appointments from './appointments'
import * as blog from './blog'

export { 
    login,
    appointments,
    blog
}