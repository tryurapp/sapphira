//React imports
import React, { useState, useRef, useEffect } from 'react'
//React native imports
import { StyleSheet, Text, View, Dimensions, Image, ScrollView, TouchableOpacity, Platform } from 'react-native'
//Native base imports
import { Fab } from 'native-base'
//Components import
import TUA_Button from '../../components/TUA_Button'
import SapphiraInput from '../../components/SapphiraInput'
import TimeRange from '../../components/TimeRange'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
//Redux imports
import { useSelector, useDispatch } from 'react-redux'
import * as SPActions from '../../redux/actions'
//Calendar imports
import CalendarPicker from 'react-native-calendar-picker'
//Images imports
import CalendarPrevious from '../../resources/appointments/calendarBack'
import CalendarNext from '../../resources/appointments/calendarNext'
import BackNewAppointment from '../../resources/appointments/backNewAppointment'
import BackNewAppointmentOff from '../../resources/appointments/backNewAppointmentOff'
//Moment import
import moment from 'moment'
import { DateTime } from 'luxon'
//Firebase imports
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/firestore'
//Generate uuid imports
import uuid from 'react-native-uuid'
//Utils imports
import * as Utils from '../../utils/'



const NewAppointment = props => {

    //Connect Redux for functional component
    const pickerCenter = useSelector(
        state => state.appointments.pickerCenter
    )
    const pickerService = useSelector(
        state => state.appointments.pickerService
    )
    const pickerTherapist = useSelector(
        state => state.appointments.pickerTherapist
    )
    const pickerCategory = useSelector(
        state => state.appointments.pickerCategory
    )
    const dateSelected = useSelector(
        state => state.appointments.dateSelected
    )
    const timeSelected = useSelector(
        state => state.appointments.timeSelected
    )
    const centersData = useSelector(
        state => state.blog.centers
    )
    const user = useSelector(
        state => state.login.userLogged
    )
    const disabledDates = useSelector(
        state => state.appointments.disabledDates
    )
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updatePickerCenter = (pickerCenter) => {
        dispatch(SPActions.appointments.updatePickerCenter(pickerCenter))
    }
    const updatePickerService = (pickerService) => {
        dispatch(SPActions.appointments.updatePickerService(pickerService))
    }
    const updatePickerTherapist = (pickerTherapist) => {
        dispatch(SPActions.appointments.updatePickerTherapist(pickerTherapist))
    }
    const updatePickerCategory = (pickerCategory) => {
        dispatch(SPActions.appointments.updatePickerCategory(pickerCategory))
    }
    const updateDateSelected = (dateSelected) => {
        dispatch(SPActions.appointments.updateDateSelected(dateSelected))
    }
    const updateTimeSelected = (timeSelected) => {
        dispatch(SPActions.appointments.updateTimeSelected(timeSelected))
    }
    const updateAppointment = (appointment, email) => {
        dispatch(SPActions.appointments.updateAppointment(appointment, email))
    }
    const getSlots = (fromDate, toDate) => {
        dispatch(SPActions.appointments.getSlots(fromDate, toDate))
    }
    
    //State
    const [seeCalendar, setSeeCalendar] = useState(false)
    const [seeTime, setSeeTime] = useState(false)
    const [therapistPressed, setTherapistPressed] = useState(false)
    const [servicePressed, setServicePressed] = useState(false)
    const [categoryPressed, setCategoryPressed] = useState(false)
    const [borderColorCenter, setBorderColorCenter] = useState('')
    const [borderColorCategory, setBorderColorCategory] = useState('')
    const [borderColorService, setBorderColorService] = useState('')
    const [borderColorTherapist, setBorderColorTherapist] = useState('')
    const [borderColorDate, setBorderColorDate] = useState('')
    const [borderColorTime, setBorderColorTime] = useState('')

    //useEffect
    useEffect( () => {
        if (user.preferredCenter) {
            updatePickerCenter({
                'name': user.preferredCenter,
                'id': ''
            })
        }
        const fromDate = firebase.firestore.Timestamp.fromDate(new Date())
        let dateToConvert = new Date(moment(new Date).add( Utils.constants.MAX_TIME_FOR_APPOINTMENT_IN_MONTHS, 'months').calendar())
        const toDate = firebase.firestore.Timestamp.fromDate(dateToConvert)
        getSlots(fromDate, toDate)
    }, [])

    //Constants
    const scrollViewRef = useRef()

    //Functions
    const saveAppointment = () => {
        if (pickerCenter && pickerCategory && pickerService && pickerTherapist && dateSelected && timeSelected) {

            let zone = ''
            centersData.forEach( center => {
                if (pickerCenter.name === center.center.centerName) {
                    zone = center.center.timeZoneName
                }
            })

            const dateToConvert = moment(`${ dateSelected } ${ timeSelected }`, "DD/MM/YYYY hh:mm a").toDate()
  
            const dateToConvertWithZone = Utils.functions.convertLocalTimeToZoneTime(dateToConvert, zone)

            const dateAppointment = firebase.firestore.Timestamp.fromDate(dateToConvertWithZone)
            
            const today = firebase.firestore.Timestamp.fromDate(new Date())
            const codeConfirmation = uuid.v1().substring(0,8)
            const appointment = {
                appointmentCreated: today ? today : '',
                appointmentMadeOn: "app",
                serviceDate: dateAppointment  ? dateAppointment  : '',
                completeCenterId: pickerCenter.id ? pickerCenter.id : '',
                centerId: pickerCenter.id ? pickerCenter.id : '',
                centerName: pickerCenter.name ? pickerCenter.name : '',
                centerZone: zone ? zone : '',
                categoryId: pickerCategory.id ? pickerCategory.id : '',
                categoryName: pickerCategory.name,
                serviceId: pickerService.id ? pickerService.id : '',
                serviceName: pickerService.name ? pickerService.name : '',
                therapistId: pickerTherapist.id ? pickerTherapist.id : '',
                therapistName: pickerTherapist.name ? pickerTherapist.name : '',
                serviceHour: timeSelected ? Utils.functions.calculateTimeNumber(timeSelected) : '',
                confirmationCode: codeConfirmation ? codeConfirmation : '',
                customerEmail: user.email ? user.email : '',
                customerName: user.name ? user.name : '',
                customerPhone: user.phone ? user.phone : '',
            }
            
            //Guardar en Firebase la cita
            updateAppointment(appointment)
        } else {
            updatePickerService('')
            Actions.SPAlert({ 
                title: `Atención.`, 
                text: 'Debe seleccionar todos los campos para poder agendar la cita.',
                setHeight: hp('4')
            })
        }
    }

    const cancelAppointment = () => {
        updatePickerCenter({})
        updatePickerCategory({})
        updatePickerService({})
        updatePickerTherapist({})
        updateDateSelected('')
        updateTimeSelected('')
        Actions.pop()
    }

    const selectCenter = () => {
        if (centersData[0]) {
            const centers = [{ 'name': 'Seleccione centro' }]
            centersData.forEach( item => {
                const data = { 
                    'name': item.center.centerName,
                    'id': item.center.centerId 
                }
                centers.push(data)
            })
            Actions.CenterPicker({ centers })
        } else {
            Actions.SPAlert({ 
                title: 'Aviso', 
                text: 'No hay ningún centro creado.',
                setHeight: hp('4')
            })
        }
        
    }

    const noCenter = () => {
        Actions.SPAlert({ 
            title: 'Aviso', 
            text: 'No hay ningún centro creado.',
            setHeight: hp('4')
        })
    }

    const selectCategory = () => {
        if (!centersData[0]) return noCenter()
        if (pickerCenter) {
            let centerSelected = {}
            centersData.forEach( item => {
                if (item.center.centerName === pickerCenter.name) {
                    centerSelected = item.center
                }
            })
            const categories = [{ 'name': 'Seleccione categoria' }]
            if (centerSelected.categories) {
                centerSelected.categories.forEach ( item => {
                    const data = { 
                        'name': item.categoryName,
                        'id': item.categoryId
                    }
                    categories.push(data)
                })
                Actions.CategoryPicker({ categories })
            } else {
                Actions.SPAlert({ 
                    title: `Atención.`, 
                    text: 'El centro seleccionado, no dispone de ninguna categoría. Seleccione otro centro, por favor',
                    setHeight: hp('4')
                })
            }
        } else {
            setCategoryPressed(true)
        }       
    }

    const selectService = () => {
        if (!centersData[0]) return noCenter()
        if (pickerCategory) {
            let centerSelected = {}
            centersData.forEach( item => {
                if (item.center.centerName === pickerCenter.name) {
                    centerSelected = item.center
                }
            })
            let categorySelected = {}
            centerSelected.categories.forEach( item => {
                if (item.categoryName === pickerCategory.name) {
                    categorySelected = item
                }
            })
            const services = [{ 'name': 'Seleccione servicio' }]
            if (categorySelected.services) {
                categorySelected.services.forEach ( item => {
                    const data = { 
                        'name': item.serviceName,
                        'id': item.serviceId 
                    }
                    services.push(data)
                })
                Actions.ServicePicker({ services })
            } else {
                Actions.SPAlert({ 
                    title: `Atención.`, 
                    text: 'El centro seleccionado, no dispone de ningún servicio. Seleccione otro centro, por favor',
                    setHeight: hp('4')
                })
            } 
        } else {
            setServicePressed(true)
        }       
    }

    const selectTherapist = () => {
        if (!centersData[0]) return noCenter()
        if(pickerService) {
            let centerSelected = {}
            centersData.forEach( item => {
                if (item.center.centerName === pickerCenter.name) {
                    centerSelected = item.center
                }
            })
            let categorySelected = {}
            centerSelected.categories.forEach( item => {
                if (item.categoryName === pickerCategory.name) {
                    categorySelected = item
                }
            })
            let serviceSelected = {}
            categorySelected.services.forEach( item => {
                if (item.serviceName === pickerService.name) {
                    serviceSelected = item
                }
            })
            const therapists = [{ 'name': 'Seleccione terapeuta' }]
            if (serviceSelected.therapists) {
                serviceSelected.therapists.forEach( item => {
                    const data = { 
                        'name': item.therapistName,
                        'id': item.therapistId
                    }
                    therapists.push(data)
                })
                Actions.TherapistPicker({ therapists })
            } else {
                Actions.SPAlert({ 
                    title: `Atención.`, 
                    text: 'El servicio seleccionado, no dispone de ningún terapeuta. Seleccione otro servicio, por favor',
                    setHeight: hp('4')
                })
            }
        } else {
            setTherapistPressed(true)
        }
    }

    const showCalendar = () => {
        if (!centersData[0]) return noCenter()
        setSeeCalendar(true)
    }

    const closeCalendar = () => {
        setSeeCalendar(false)
        updateDateSelected('')
    }

    const selectDate = date => {
        const dateString = `${ date.format('MM/DD/YYYY') } 01:00`
        const newDate = moment(dateString)
        const today = moment()
        if (newDate > today) {
            updateDateSelected(date.format('DD/MM/YYYY'))
            setSeeCalendar(false)
        } else {
            setSeeCalendar(true)
            Actions.SPAlert({ 
                title: `Atención.`, 
                text: 'Para poder agendar, la fecha debe ser mayor que hoy.',
                setHeight: hp('4')
            })
        }
    }

    const showTime = () => {
        if (!centersData[0]) return noCenter()
        setSeeTime(true)
    }

    const validateFormCenter = () => {
        if (pickerCenter) setBorderColorCenter('#CBAC77')
    }

    const validateFormCategory = () => {
        if (pickerCategory) setBorderColorCategory('#CBAC77')
    }

    const validateFormService = () => {
        if (pickerService) setBorderColorService('#CBAC77')
    }

    const validateFormTherapist = () => {
        if (pickerTherapist) setBorderColorTherapist('#CBAC77')
    }

    const validateFormDate = () => {
        if (dateSelected) setBorderColorDate('#CBAC77')
    }

    const validateFormTime = () => {
        if (timeSelected) {
            setBorderColorTime('#CBAC77')
            setSeeTime(false)
        }
    }

    //Render components
    const renderDate = () => {
        return(
            <View> 
                {
                    seeCalendar
                    ?
                        pickerTherapist
                        ?
                        <View>
                            <View style={{ height: hp('1.5') }} />
                            <View style={ styles.viewCancel }>
                                <TouchableOpacity
                                    transparent
                                    onPress={ closeCalendar }
                                >
                                    <Text style={ styles.textCancel }>Cancelar</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: hp('1.5') }} />
                            <View style={{ alignItems: 'center', marginLeft: wp('-7.44') }}>
                            <CalendarPicker 
                                onDateChange={ date => selectDate(date) }
                                previousComponent={ <CalendarPrevious /> }
                                nextComponent={ <CalendarNext /> } 
                                width={ Dimensions.get('window').width - wp('14.96') }
                                todayBackgroundColor={ '#CBAC77' }
                                selectedDayColor={ '#CBAC77' }
                                weekdays={ ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'] }
                                startFromMonday={ true }
                                months={ ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'] }
                                textStyle={ styles.calendarTextStyle }
                                selectYearTitle='Seleccione año  '
                                selectMonthTitle='Seleccione mes  '
                                todayTextStyle={ styles.todayTextStyle }
                                disabledDates={ disabledDates }
                                disabledDatesTextStyle={ styles.disabledDateStyle }
                                minDate={ moment(new Date) }
                                maxDate={ moment(new Date).add(Utils.constants.MAX_TIME_FOR_APPOINTMENT_IN_MONTHS, 'months').calendar() }
                            />
                            </View>
                        </View>
                        :
                        <Text style={ styles.textWarning }>Debe seleccionar un terapeuta</Text>
                    :
                    null
                }
            </View>
        )
    }

    return (
        <View style={ styles.container }>
            <View>
                {
                    Platform.OS === 'android' 
                    ? 
                    null
                    : 
                        <Fab 
                            height={ hp('3') }
                            width={ hp('3') }
                            top = { hp('5.4') }
                            left = { wp('1.8') }
                            backgroundColor='#FFFFFF30'
                            icon={ seeCalendar ? <BackNewAppointmentOff/> : <BackNewAppointment/> }
                            onPress={ cancelAppointment }
                        />
                }
            </View>
            <ScrollView
                style={ styles.scroll }
                ref={ scrollViewRef }
                onContentSizeChange={ () => scrollViewRef.current.scrollToEnd({ animated: true })}
            >
                <Image 
                    source={ require('../../resources/appointments/appointmentsPhoto.png') }
                    style={ styles.image }
                    resizeMode='cover'
                />
                <View style={{ height: Platform.OS === 'android' ? hp('0.5') : hp('5.2') }} />
                <View style={{ flexDirection: 'row' }}>
                {
                    Platform.OS === 'android' 
                    ? 
                        <TouchableOpacity 
                            transparent
                            style={ styles.back }
                            onPress={ cancelAppointment }
                        >
                            <BackNewAppointmentOff />
                        </TouchableOpacity>
                    : 
                        null
                }
                <Text style={ styles.text }>Nueva cita</Text>
                </View>
                <View style={{ height: Platform.OS === 'android' ? hp('2.5') : hp('5.5'), marginLeft: wp('7.44') }} />
                <SapphiraInput 
                    label='Centro Sapphira Privé'
                    placeholder={ user.preferredCenter ? user.preferredCenter : 'Centro Sapphira Privé' }
                    value={ pickerCenter.name }
                    onChangeText={ () => {} }
                    onFocus={ selectCenter }
                    keyboardType='default'
                    onSubmitEditing={ validateFormCenter }
                    onEndEditing={ validateFormCenter }
                    multiline={ false} 
                    isPassword={ false }
                    borderColor={ borderColorCenter }
                    editable={ false }
                />
                <View style={{ height: hp('2.7') }} />
                <SapphiraInput 
                    label='Categoría'
                    placeholder='Categoría'
                    value={ pickerCategory.name }
                    onChangeText={ () => {} }
                    onFocus={ selectCategory }
                    keyboardType='default'
                    onSubmitEditing={ validateFormCategory }
                    onEndEditing={ validateFormCategory }
                    multiline={ false} 
                    isPassword={ false }
                    borderColor={ borderColorCategory }
                    editable={ false }
                />
                {
                    (!pickerCenter && categoryPressed)
                    ?
                    <Text style={ styles.textWarning }>Debe seleccionar un centro</Text>
                    :
                    null
                }
                <View style={{ height: hp('2.7') }} />
                <SapphiraInput 
                    label='Servicio'
                    placeholder='Servicio'
                    value={ pickerService.name }
                    onChangeText={ () => {} }
                    onFocus={ selectService }
                    keyboardType='default'
                    onSubmitEditing={ validateFormService }
                    onEndEditing={ validateFormService }
                    multiline={ false} 
                    isPassword={ false }
                    borderColor={ borderColorService }
                    editable={ false }
                />
                {
                    (!pickerCategory && servicePressed)
                    ?
                    <Text style={ styles.textWarning }>Debe seleccionar una categoría</Text>
                    :
                    null
                }
                <View style={{ height: hp('2.7') }} />
                <SapphiraInput 
                    label='Terapeuta'
                    placeholder='Terapeuta'
                    value={ pickerTherapist.name }
                    onChangeText={ () => {} }
                    onFocus={ selectTherapist }
                    keyboardType='default'
                    onSubmitEditing={ validateFormTherapist }
                    onEndEditing={ validateFormTherapist }
                    multiline={ false} 
                    isPassword={ false }
                    borderColor={ borderColorTherapist }
                    editable={ false }
                />
                {
                    (!pickerService && therapistPressed)
                    ?
                    <Text style={ styles.textWarning }>Debe seleccionar un servicio</Text>
                    :
                    null
                }
                <View style={{ height: hp('2.7') }} />
                <SapphiraInput 
                    label='Fecha'
                    placeholder='Fecha'
                    value={ dateSelected }
                    onChangeText={ () => {} }
                    onFocus={ showCalendar }
                    keyboardType='default'
                    onSubmitEditing={ validateFormDate }
                    onEndEditing={ validateFormDate }
                    multiline={ false} 
                    isPassword={ false }
                    borderColor={ borderColorDate }
                    editable={ false }
                />
                {
                    renderDate()
                }
                <View style={{ height: hp('2.7') }} />
                <SapphiraInput 
                    label='Hora'
                    placeholder='Hora'
                    value={ timeSelected }
                    onChangeText={ () => validateFormTime() }
                    onFocus={ showTime }
                    keyboardType='default'
                    onSubmitEditing={ validateFormTime }
                    onEndEditing={ validateFormTime }
                    multiline={ false} 
                    isPassword={ false }
                    borderColor={ borderColorTime }
                    editable={ false }
                />
                {
                    seeTime
                    ?
                    <View>
                        <TimeRange />
                    </View>
                    : null
                }
                {
                    seeTime 
                    ?
                    <View style={{ height: hp('2') }} />
                    :
                    <View style={{ height: hp('10.6') }} />
                }
                {
                    timeSelected
                    ?
                    <TUA_Button
                        bgColor={ '#CBAC77' }
                        label={ 'Agendar' }
                        labelColor={ '#FFFFFF' }
                        setWidth={ Dimensions.get('window').width - wp('14.96') }
                        onPress={ saveAppointment }
                        fontSize={ hp('1.7') }
                        setActivity={ false }
                        setHeight={ hp('5.5') }
                        buttonStyle={{ borderRadius: hp('0.5'), marginLeft: wp('7.44') }}
                        //borderWidth={ wp('0.3') }
                    />
                    :
                    null
                }
                <View style={{ height: hp('5') }} />
            </ScrollView>
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: { 
        flex: 1,
        backgroundColor: '#F9F9F9',
    },
    backIcon: {
        position: 'absolute',
        top: hp('5.4'),
        left: wp('5.84'),
    },
    scroll: {
    },
    text: {
        fontFamily: 'Mr Lackboughs',
        fontSize: Platform.OS === 'android' ? hp('5.5') : hp('4.5'),
        marginLeft: wp('10'),
        paddingLeft: wp('2.5'),
    },
    image: {
        width: Dimensions.get('window').width,
        height: hp('21'),
    },
    labelText: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.3'),
        color: '#707070',
        marginLeft: wp('10'),
        marginBottom: hp('1'),
    },
    viewCancel: {
        alignItems: 'flex-end',
        marginRight: wp('10'),
    },
    textCancel: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
        color: '#CBAC77',
    },
    textWarning: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#FF3A20',
        marginLeft: wp('10'),
    },
    calendarTextStyle: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
        color: '#000000',
    },
    todayTextStyle: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
        color: '#F9F9F9',
    },
    disabledDateStyle: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
    },
    back: {
        marginTop: hp('1'),
        marginLeft: wp('5'),
    }
  })

  export default NewAppointment