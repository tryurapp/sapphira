import * as types from '../types'
//Firebase imports
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/firestore'


export function updateWPFeatured(wpFeatured) {
    //console.log('UpdateWPFeatured: ', wpFeatured)
    return {
        type: types.BLOG_UPDATE_WP_FEATURED,
        wpFeatured
    }
}

export function updateWPPosts(wpPosts) {
    //console.log('UpdateWPPosts: ', wpPosts)
    return {
        type: types.BLOG_UPDATE_WP_POSTS,
        wpPosts
    }
}

// Los posts destacados en WP se les ha dado la categoria 159
export function getWPPosts() {
    return (dispatch, getState) => {
        fetch(`https://www.centroestefl.com/wp-json/wp/v2/posts?_embed`)
        .then( response => response.json())
        .then( responseJson => {
            let wpFeatured = []
            dispatch(updateWPPosts(responseJson))
            responseJson.forEach( post => {
                const categories = post.categories
                categories.forEach( category => {
                    if (category === 159) wpFeatured.push(post)
                })
            })
            dispatch(updateWPFeatured(wpFeatured))
        })
        .catch( error => {
            console.error(error)
        })
    }
}

export function updateVersion(version) {
    return {
        type: types.UPDATE_VERSION,
        version
    }
}

//Get Version info
export function getVersion() {
    return (dispatch, getState) => {
        const state = getState()
        firebase.firestore().collection('Version')
        .onSnapshot( query => {
            if (query) {
                const versionReaded = []
                query.forEach( doc => {
                    versionReaded.push(doc.data())
                })
                const ver = Platform.OS === 'ios' ? versionReaded[0].iosVersion : versionReaded[0].androidVersion
                dispatch(updateVersion(ver))
            }
        })
    }
}