//React imports
import React, { useEffect } from 'react';
//React native imports
import { StyleSheet } from 'react-native';
//Components imports
import Navigation from './navigation/navigation'
//Redux and redux-Persist imports
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import * as reducers from './redux/reducers'
import { PersistGate } from 'redux-persist/lib/integration/react'
import { NativeBaseProvider } from 'native-base';
import { LogBox } from 'react-native'


//Constants
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2,
}
const rReducer = combineReducers(reducers)
const pReducer = persistReducer(persistConfig, rReducer)
const store = createStore(
  pReducer,
  applyMiddleware(thunk)
)
const persistor = persistStore(store)

//Render
const App = () => {
  useEffect( () => {
    console.error = error => error.apply
    LogBox.ignoreAllLogs()
}, [])
  return (
    <NativeBaseProvider>
      <Provider store={ store }>
        <PersistGate persistor={ persistor }>
          <Navigation />
        </PersistGate>
      </Provider>
    </NativeBaseProvider>
  );
};

//Styles
const styles = StyleSheet.create({
});

export default App
