//React imports
import React, { useState } from 'react';
//React native imports
import { View, StyleSheet, Text, TouchableOpacity, Dimensions, TextInput, Keyboard, Platform } from 'react-native';
//Images imports
import SeePass from '../resources/login/seePass'
import NotSeePass from '../resources/login/notSeePass'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'



const SapphiraInput = props => {

    //State
    const [focus, setFocus] = useState(false)
    const [seePasswordStatus, setSeePasswordStatus] = useState(isPassword)

    //Constants
    const { onChangeText=() => {}, onChange=() => {}, onSubmitEditing=() => {}, onEndEditing=() => {},value, placeholder, isPassword=false, keyboardType='default', multiline=false, label, error, warning, editable=true } = props

    //Functions
    const changeFocus = () => {
        if (!editable) Keyboard.dismiss()
        if (props.onFocus) {
            props.onFocus()
            setFocus(false)
        } else {
            if (!value || value == '') {
                setFocus(true)
            }
        }
    }

    const onEndEditingInput = () => {
        setFocus(false)
        onEndEditing()
    }

    let totalHeight = 5.2
    if (focus || value) totalHeight = totalHeight + 1.4
    if (error || warning) totalHeight = totalHeight + 1.4
    const totalHeightString = totalHeight.toString()

    //Renders
    return(
        <View style={{ height: hp(totalHeightString) }}>
            {
                (focus || value)
                ?
                <Text style={styles.label }>{ label }</Text>
                :
                null
            }
            <View 
                style={ [styles.inputStyle, { borderColor: props.borderColor ? props.borderColor : '#E2E2E2' }] }
            >
                <View style={ styles.value }>
                    <TextInput
                        style={ styles.textInput }
                        autoCapitalize='none'
                        onChangeText={ (v) => onChangeText(v) }
                        onFocus={ () => changeFocus() }
                        onChange={ () => onChange() }
                        onSubmitEditing={ ()=> onSubmitEditing()}
                        onEndEditing={ () => onEndEditingInput() }
                        value={ value }
                        placeholder={ placeholder }
                        placeholderTextColor={ '#707070' }
                        keyboardType={ keyboardType }
                        secureTextEntry={ seePasswordStatus ?  true : false }
                        multiline={ multiline }
                    />
                </View>
                {
                    isPassword
                    ?
                    <TouchableOpacity
                        transparent
                        onPress={ () => setSeePasswordStatus(!seePasswordStatus) }
                    >
                        {
                            seePasswordStatus ? <SeePass /> : <NotSeePass />
                        } 
                    </TouchableOpacity>
                    :
                    null
                } 
            </View>
            {
                (error || warning)
                ?
                    error 
                    ?
                    <View style={ [styles.message, { alignItems: 'flex-end', marginLeft: wp('-1') }] }>
                        <Text style={ styles.error }>{ error }</Text>
                    </View>
                    :
                    <View style={ styles.message }>
                        <Text style={ [styles.warning, { marginLeft: wp('1') }] }>{ warning }</Text>
                    </View>
                :
                null
            }
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    inputStyle: {
        backgroundColor: '#CBAC771C',
        width: Dimensions.get('window').width - wp('14.96'),
        height: Platform.OS === 'android' ? hp('5.8') : hp('5.2'),
        borderColor: '#E2E2E2',
        borderRadius: hp('0.55'),
        borderWidth: wp('0.2'),
        alignItems: 'flex-start',
        color: '#707070',
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: wp('7.44'),
    },
    value: {
        width: '87%',
        marginLeft: wp('2'),
        
    },
    textInput: {
        fontFamily: 'Helvetica Neue Medium',
        fontSize: hp('1.85'),
        color: '#222427',
    },
    label: {
        width: Dimensions.get('window').width - wp('14,96'),
        marginLeft: wp('1'),
        marginBottom: wp('1'),
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.3'),
        color: '#707070',
        marginLeft: wp('7.44'),
    },
    error: {
        color: '#FF3A20',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.3'),
        marginLeft: wp('7.44'),
    },
    warning: {
        color: '#707070',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.3'),
        marginLeft: wp('7.44'),
    },
    message: {
        width: Dimensions.get('window').width - wp('20'),
        marginTop: wp('1'),
        marginLeft: wp('7.44'),
    },
})

export default SapphiraInput