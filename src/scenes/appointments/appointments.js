//React imports
import React, { useState, useRef, useEffect, useCallback } from 'react'
//React native imports
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Dimensions, AppState } from 'react-native'
//Images import
import PastAppointments from '../../resources/appointments/pastAppointments'
import UpcomingAppointments from '../../resources/appointments/upcomingAppointments'
import NewAppointmentImage from '../../resources/appointments/newAppointmentImage'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
//Redux imports
import { useSelector, useDispatch } from 'react-redux'
import * as SPActions from '../../redux/actions'
//Moment imports
import moment from 'moment'
//Utils imports
import * as Utils from '../../utils/'



const Appointments = (props) => {

    //Connect Redux for functional component
    //mapStateToProps
    const upcomingAppointments = useSelector(
        state => state.appointments.upcomingAppointments
    )
    const pastAppointments = useSelector(
        state => state.appointments.pastAppointments
    )
    const user = useSelector(
        state => state.login.userLogged
    )
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const getSlots = () => {
        dispatch(SPActions.appointments.getSlots())
    }
    const getCentersData = () => {
        dispatch(SPActions.appointments.getCentersData())
    }

    //State
    const [dataUpdated, setDataUpdated] = useState(false)

    const appStateRef = useRef(AppState.currentState)

    const handleAppState = nextAppState => {
        if (appStateRef.current === 'background' && nextAppState === 'active') {
            getSlots()
        }
        appStateRef.current = nextAppState
        //getSlots()
    }

    //UseEffect
    useEffect( () => {
        getSlots()
        getCentersData()
        AppState.addEventListener("change", handleAppState)
        return () => {
            AppState.removeEventListener("change", handleAppState)
        }
    }, [])

    //Constants
    const flatListRef = useRef()

    //Functions
    const newAppointment = () => {
        setDataUpdated(false)
        Actions.NewAppointment()
    }

    const showAppointment = (appointment) => {
        Actions.replace('SavedAppointment', { appointment: appointment, type: 'noSend' })
    }

    if (user.email === 'invited') {
        Actions.replace('_Blog')
        Actions.SPAlert({ 
            title: `Estas invitado`, 
            text: 'No puedes crear citas.',
            setHeight: hp('4')
        })
    }
    
    //Renders
    function renderItem(item) {
        const date = moment(item.serviceDate.seconds * 1000).format('DD/MM/YYYY')

        const timeOfAppointment = Utils.functions.calculateTime(item.serviceHour)

        return(
            <View style={ styles.viewItem }>
                <TouchableOpacity
                    transparent
                    onPress={ () => showAppointment(item) }
                >
                    <View style={{ height: hp('1.63') }} />
                    <Text style={ styles.textItem1 }>{ date } - { timeOfAppointment }</Text>
                    <View style={{ height: hp('1.08') }} />
                    <Text style={ styles.textItem }>{ item.centerName } - { item.serviceName }</Text>
                    <View style={{ height: hp('1.08') }} />
                </TouchableOpacity>
            </View>
        )
    }

    const renderSeparator = () => {
        return(
            <View style={{
                height: hp('0.1'),
                backgroundColor: '#E2E2E2',
                width: wp('63'),
                marginLeft: wp('24'),
            }} />
        )
    }

    return (
        <View style={ styles.container }>
            <View style={{ height: hp('6.9') }} />
            <View style={ styles.viewNewAppointment }>
                <TouchableOpacity
                    transparent
                    onPress={ newAppointment }
                >
                    <NewAppointmentImage />
                </TouchableOpacity>
            </View>
            <Text style={ styles.text }>Mis citas</Text>
            <View style={{ height: hp('5.08') }} />
            <View style={ styles.viewAppointments }>
                <UpcomingAppointments />
                <Text style={ styles.textTitle }>Próximas citas</Text>
            </View>
            {
                (!upcomingAppointments[0])
                ?
                <View>
                    <View style={{ height: hp('3.24') }} />
                    <Text style={ styles.textAppointment }>No hay futuras citas</Text>
                    <View style={{ height: hp('1.08') }} />
                    {
                        renderSeparator()
                    }
                </View>
                :
                <FlatList 
                    style={ styles.flatListStyle }
                    data={ upcomingAppointments }
                    renderItem={ item => renderItem(item.item)}
                    extraData={ dataUpdated }
                    keyExtractor={ (item, index) => index.toString() }
                    ItemSeparatorComponent={ renderSeparator }
                    ref={ flatListRef }
                />
            }
            <View style={{ height: hp('5,08') }} />
            <View style={ styles.viewAppointments }>
                <PastAppointments />
                <Text style={ styles.textTitle }>Citas pasadas</Text>
            </View>
            
            {
                (!pastAppointments[0])
                ?
                <View>
                    <View style={{ height: hp('3.24') }} />
                    <Text style={ styles.textAppointment }>No hay citas pasadas</Text>
                    <View style={{ height: hp('1.08') }} />
                    {
                        renderSeparator()
                    }
                </View>
                :
                <FlatList 
                    style={ styles.flatListStyle }
                    data={ pastAppointments }
                    renderItem={ item => renderItem(item.item)}
                    extraData={ dataUpdated }
                    keyExtractor={ (item, index) => index.toString() }
                    ItemSeparatorComponent={ renderSeparator }
                />
            }
            <View style={{ height: hp('5') }} />
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        height: Dimensions.get('window').height - hp('10'),
        backgroundColor: '#F9F9F9',
    },
    text: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.5'),
        marginLeft: wp('10'),
        paddingLeft: wp('2.5'),
    },
    textTitle: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('2'),
        color: '#707070',
        marginLeft: wp('3.54'),
        marginTop: hp('0.98'),
    },
    viewAppointments: {
        flexDirection: 'row',
        marginLeft: wp('10'),
    },
    textAppointment: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#707070',
        marginLeft: wp('24'),
    },
    viewNewAppointment: {
        alignItems: 'flex-end',
        marginRight: wp('10'),
    },
    viewItem: {
        flex: 1,
        width: wp('90'),
    },
    viewLineItem: {
        flexDirection: 'row',
    },
    textItem1: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.63'),
        color: '#222427',
        marginLeft: wp('24'),
    },
    textItem: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#707070',
        marginLeft: wp('24'),
    },
    flatListStyle: {
        height: hp('19.66'),
    },
  })

  export default Appointments