//React imports
import React from 'react'
//React native imports
import { View, StyleSheet, Text } from 'react-native'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
// Images imports
import TabBarAppointmentsOn from '../resources/tabBar/tabBarAppointmentsOn'
import TabBarAppointmentsOff from '../resources/tabBar/tabBarAppointmentsOff'


const TabIconAppointments = props => {

    //Functions
    let imageIcon = true
    if (props.focused) {
        imageIcon = true
    } else {
        imageIcon = false
    }
        
    //Renders
    return(
        <View style={ styles.container }>
            {
                imageIcon ?
                    <TabBarAppointmentsOn
                        width = { wp('12') }
                        height = { hp('20') }
                    />
                :
                    <TabBarAppointmentsOff
                        width = { wp('12') }
                        height = { hp('20') }
                    />
            }
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container:{
        flex: 1, 
        flexDirection: 'column', 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: wp('2'),
    },
});

export default TabIconAppointments