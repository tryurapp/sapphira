import * as types from '../types'
//Firebase imports
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/firestore'
import { Actions } from 'react-native-router-flux'
import { getState } from 'jest-circus'



export const updateUserToRemember = (userToRemember) => {
    return {
        type: types.UPDATE_USER_TO_REMEMBER,
        userToRemember
    }
}

//Get userLogged
export function getUserLoggedData(email) {
    return (dispatch, getState) => {
        const state = getState()
        firebase.firestore().collection('Users')
        .where('email', '==', email)
        .get()
        .then( query => {
            if (query) {
                const items = []
                query.forEach( doc => {
                    items.push(doc.data())
                })
                dispatch(updateUserLogged(items[0]))
            }
        })
        .catch( error => {
            console.log('ErrorGettingUserLoggedData: ', error.message)
        })
    }
}

export const updateUserLogged = (userLogged) => {
    return {
        type: types.LOGIN_UPDATE_USER_LOGGED,
        userLogged
    }
}

export function updateUserLoggedData(user) {
    return (dispatch, getState) => {
        const state = getState()
        firebase.firestore().collection('Users')
        .where('email', '==', user.email)
        .get()
        .then( query => {
            if (query) {
                const userId = []
                query.forEach( doc => {
                    userId.push(doc.id)
                })
                firebase.firestore().collection('Users').doc(userId[0])
                .update(user)
                .then( refDoc => {
                    //dispatch(updateUserLogged(user))
                    dispatch(getUserLoggedData(user.email))
                })
                .catch( error => {
                    dispatch(addUserToFirebase(user))
                })
            }
        })
        .catch( error => {
            console.log('ErrorUpdatingUserLoggedData: El User no existe. Creo el user')
            dispatch(addUserToFirebase(user))
        })
    }
}

export function addUserToFirebase(user) {
    return (dispatch, getState) => {
        const db = firebase.firestore().collection('Users')
        //Ask for user
        db.where("email", "==", user.email)
            .get()
            .then( data => {
                const dataReaded = []
                data.forEach( doc => {
                    dataReaded.push(doc.id)
                })
                //If user exits, update user
                if (dataReaded[0]){
                    db.doc(dataReaded[0]).update(user)
                        .then( refDoc => {
                            Actions.replace("_Blog", { email })
                            Actions.SPAlert({ 
                                title: `Bienvenido ${user.email}`, 
                                text: 'Has sido añadido a nuestra comunidad',
                                setHeight: hp('4')
                            })
                        })
                        .catch( error => {
                            console.log('Error: ', error)
                        })
                } 
                //If user not exits, create user
                else {
                    db.add(user)
                        .then( refDoc => {
                            Actions.replace("_Blog", { email })
                            Actions.SPAlert({ 
                                title: `Bienvenido ${user.email}`, 
                                text: 'Has sido añadido a nuestra comunidad',
                                setHeight: hp('4')
                            }) 
                        })
                        .catch( error => {
                            console.log('Error: ', error)
                        })
                }
            })
            .catch( error => {
                console.log('Error: ', error)
            })
    }
}