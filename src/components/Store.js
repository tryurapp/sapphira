//React imports
import React from 'react'
//React native imports
import { View, StyleSheet } from 'react-native'
//WebView imports
import { WebView } from 'react-native-webview'



const Store = () => {
    console.log('En Store')
    //Renders
    if (Platform.OS === 'ios') {
        console.log('Store iOS')
        return (
            <WebView 
                source={{ uri: 'https://www.tryurapp.com' }} 
                marginTop={ 40 }
            />
        )
    } else {
        console.log('store Android')
        return(
            <WebView 
                source={{ uri: 'https://play.google.com/store/apps/details?id=com.sapphira' }} 
            />
        )
    }
}

//Styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#e2e2e2CC',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
})

export default Store