//React imports
import React, { useState } from 'react'
//React native imports
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
//Components imports
import TUA_Button from './TUA_Button'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React native router flux imports
import { Actions } from 'react-native-router-flux'
//Picker imports
import { Picker } from '@react-native-picker/picker'
//Redux imports
import { useSelector, useDispatch } from 'react-redux'
import * as SPActions from '../redux/actions'



const CenterPicker = props => {

    //Connect Redux for functional component
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updatePickerCenter = (pickerCenter) => {
        dispatch(SPActions.appointments.updatePickerCenter(pickerCenter))
    }

    //State
    const [selectedCenter, setSelectedCenter] = useState()
    const [selectedCenterIndex, setSelectedCenterIndex] = useState('')

    //Constants
    let { centers } = props
    
    //Functions
    const itemSelected = () => {
        if (selectedCenterIndex !== '0' && selectedCenterIndex) {
            let id = ''
            centers.forEach( element => {
                if (element.name === selectedCenter) {
                    id = element.centerId
                }
            });
            updatePickerCenter({
                'name': selectedCenter,
                'id': id
            })
        }
        Actions.pop( { refresh: {} } )
    }

    //Renders
    return(
        <View style={ styles.container }>
            <View style={{ height: hp('1.5') }} />
            <View style={ styles.viewCancel }>
                <TouchableOpacity
                    transparent
                    onPress={ () => Actions.pop({ refresh: {} }) }
                >
                    <Text style={ styles.textCancel }>Cancelar</Text>
                </TouchableOpacity>
            </View>
            <Picker
                selectedValue= { selectedCenter }
                onValueChange={ (itemValue, itemIndex) =>  {
                        setSelectedCenter(itemValue)
                        setSelectedCenterIndex(itemIndex.toString()) 
                    }
                }   
            >
                { 
                    Object.keys(centers).map( key => {
                        return (
                            <Picker.Item 
                                label={ centers[key].name } 
                                value={ centers[key].name }
                                key={ key } 
                            />
                        )
                    })
                }
            </Picker>
            <View style={ styles.button }>
                <TUA_Button
                    bgColor={ '#CBAC77' }
                    label={ 'Listo' }
                    labelColor={ '#FFFFFF'}
                    setWidth={ wp('80') }
                    onPress={ itemSelected }
                    fontSize={ hp('1.7') }
                    setActivity={ false }
                    setHeight={ hp('5.5') }
                    buttonStyle={{ borderRadius: hp('0.5'),}}
                />
            </View>
            <View style={{ height: hp('4') }} />
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#E2E2E2',
        position: 'absolute',
        top: Dimensions.get('window').height / 2,
        bottom: 0,
        left: 0,
        right: 0,
        borderRadius: wp('4')
    },
    button: {
        alignItems: 'center',
        marginTop: hp('3'),
    },
    viewCancel: {
        alignItems: 'flex-end',
        marginRight: wp('10'),
    },
    textCancel: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
        color: '#CBAC77',
    },
})

export default CenterPicker