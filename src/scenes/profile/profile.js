//React imports
import React, { useState, useEffect } from 'react'
//React native imports
import { StyleSheet, Text, View, Platform, PermissionsAndroid, Dimensions, TouchableOpacity, Image } from 'react-native'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//Redux imports
import { useSelector, useDispatch } from 'react-redux'
import * as SPActions from '../../redux/actions'
//react native router flux imports
import { Actions } from 'react-native-router-flux'
//Firebase imports
import firebase from '@react-native-firebase/app'
import auth from '@react-native-firebase/auth'
import storage from '@react-native-firebase/storage'
//React native image picker imports
import { launchCamera, launchImageLibrary } from 'react-native-image-picker'
//Images import
import AvatarDefault from '../../resources/profile/avatarDefault'
import Phone from '../../resources/profile/phone'
import Address from '../../resources/profile/address'
import Favourites from '../../resources/blog/favouriteOn'
import Camera from '../../resources/profile/camera'
import Exit from '../../resources/profile/Exit'
import Edit from '../../resources/profile/Edit'
import DeleteAccount from '../../resources/profile/deletaAccountSap'
//Phone calls imports
import call from 'react-native-phone-call'
//Utils imports
import * as Utils from '../../utils/'
import moment from 'moment'


const Profile = props => {

    //Connect Redux for functional component
    //mapStateToProps
    const user = useSelector(
        state => state.login.userLogged
    )
    const lastAppointment = useSelector(
        state => state.appointments.lastAppointment
    )
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updateUserLogged = (userLogged) => {
        dispatch(SPActions.login.updateUserLogged(userLogged))
    }
    const updateUserToRemember = (userToRemember) => {
        dispatch(SPActions.login.updateUserLogged(userToRemember))
    }
    const updateUpcomingAppointments = (upcomingAppointments) => {
        dispatch(SPActions.appointments.updateUpcomingAppointments(upcomingAppointments))
    }
    const updatePastAppointments = (pastAppointments) => {
        dispatch(SPActions.appointments.updatePastAppointments(pastAppointments))
    }
    const updateLastAppointment = (lastAppointment) => {
        dispatch(SPActions.appointments.updateLastAppointment(lastAppointment))
    }
    const getSlots = () => {
        dispatch(SPActions.appointments.getSlots())
    }

    //State
    const [avatar, setAvatar] = useState(null)
    const [userAvatar, setUserAvatar] = useState(false)
    const [avatarLoaded, setAvatarLoaded] = useState(null)

    //useEffect
    useEffect( () => {
        const path = `avatars/${ user.email }`
        firebase.storage().ref().child(path).getDownloadURL()
        .then( (url) => {
            setAvatarLoaded(url)
            setUserAvatar(true)
        })
        .catch( (error) => {
            setAvatarLoaded(null)
            setUserAvatar(false)
            switch (error.code) {
                case 'storage/object-not-found':
                    console.log('storage/object-not-found')
                    break
                case 'storage/unauthorized':
                    console.log('storage/unauthorized')
                    break
                case 'storage/canceled':
                    console.log('storage/canceled')
                    break
                case 'storage/unknown':
                    console.log('storage/unknown')
                    break
            }
        })
        //getAppointments()
        getSlots()
    }, [])

    //Functions
    const takePhoto = () => {
        const options = {
            mediaType: 'photo',
            quality: 1,
            savePhotos: true,
        }
        launchCamera(options, response => {
            if (response.didCancel) {
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: 'Cancelado por el usuario.',
                    setHeight: hp('4')
                })
            } else if (response.errorCode === 'camera_unavailable') {
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: 'Cámara no disponible en su dispositivo.',
                    setHeight: hp('4')
                })
            } else if (response.errorCode === 'permission') {
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: 'No ha concedido permiso para usar la cámara.',
                    setHeight: hp('4')
                })
            } else if (response.errorCode === 'others') {
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: `${ error.message }`,
                    setHeight: hp('4')
                })
            } else {
                const avatarSrc = { uri: response.assets[0].uri }
                setAvatar(avatarSrc)
                setUserAvatar(true)
                uploadImage(avatarSrc)
            }
        })
    }
    
    const getPhoto = () => {
        const options = {
            mediaType: 'photo',
            maxWidth: hp('14'),
            maxHeight: hp('14'),
            quality: 1,
        }
        launchImageLibrary(options, response => {
            if (response.didCancel) {
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: 'Cancelado por el usuario.',
                    setHeight: hp('4')
                })
            } else if (response.errorCode === 'camera_unavailable') {
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: 'Cámara no disponible en su dispositivo.',
                    setHeight: hp('4')
                })
            } else if (response.errorCode === 'permission') {
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: 'No ha concedido permiso para usar la cámara.',
                    setHeight: hp('4')
                })
            } else if (response.errorCode === 'others') {
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: `${ error.message }`,
                    setHeight: hp('4')
                })
            } else {
                const avatarSrc = { uri: response.assets[0].uri }
                setAvatar(avatarSrc)
                setUserAvatar(true)
                //Aqui envio la imagen a storage            
                uploadImage(avatarSrc)
            }
        })
    }

    const getAvatar = () => {
        Actions.SPAlert2({ 
            title: `Selecciona avatar.`, 
            text: `Selecciona de donde coger el avatar`,
            setHeight: hp('4'),
            onPress1: () => getPhoto() ,
            onPress2: () => takePhoto(),
            labelButton1: 'Carrete',
            labelButton2: 'Cámara',
        })
    }

    const uploadImage = async (image) => {
        const { uri } = image
        const filename = `avatars/${ user.email }`
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
        const task = storage()
            .ref(filename)
            .putFile(uploadUri)
        
        try {
            await task
        } catch (e) {
            console.error(e)
        }
        setAvatarLoaded(null)
        Actions.SPAlert({ 
            title: `Avatar almecenado`, 
            text: 'Su avatar se ha almacenado',
            setHeight: hp('4')
        })
    }

    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: 'Permiso de uso de cámara',
                    message: `Sapphira Privé necesita hacer uso de tu cámara para seleccionar tu avatar`,
                    //buttonNeutral: "Salir",
                    buttonNegative: "No permitir",
                    buttonPositive: 'PERMITIR'
                },
                
            )
            console.log('Granted: ', granted)
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                getAvatar()
            } else {
                Actions.SPAlert({ 
                    title: `Atención.`, 
                    text: `Permiso para acceder a su cámara denegado`,
                    setHeight: hp('4')
                })
            }
            } catch(err) {
                console.warn(err)
            }
        } else {
            getAvatar()
        }
    }

    const exitWithoutSignOut = () => {
        Actions.replace('WelcomeScreen')
    }

    const logout = () => {
        if (user.email === 'invited') {
            exitWithoutSignOut()
        } else {
            //Delete User Parameters
            updateUserLogged({})
            updatePastAppointments([])
            updateUpcomingAppointments([])
            updateLastAppointment('')
            auth().signOut()
            .then( () => Actions.replace('WelcomeScreen') )
            .catch( () => Actions.replace('WelcomeScreen') )
        }
    }

    const edit = () => {
        Actions.UserForm({ user,avatarLoaded })
    }

    const makeCall = () => {
        if (user.phoneCenter) {
            const args = {
                number: user.phoneCenter,
                prompt: true
            }
            call(args).catch(console.error)
        } else {
            Actions.SPAlert({ 
                title: 'Aviso', 
                text: 'No hay centro seleccionado',
                setHeight: hp('4')
            })
        }
    }

    const showAddress = () => {
        if (user.preferredCenter) {
            Actions.SPAlert({ 
                title: user.preferredCenter.name, 
                text: user.addressCenter,
                setHeight: hp('4')
            })
        } else {
            Actions.SPAlert({ 
                title: 'Aviso', 
                text: 'No hay centro seleccionado',
                setHeight: hp('4')
            })
        }
    }

    const goToFavourites = () => {
        Actions.Favourites()
    }

    const showLastAppointmentDate = (timestamp, hour) => {
        if (timestamp) {
            const date = moment(timestamp._seconds * 1000).format('DD/MM/YYYY')
            const timeOfAppointment = Utils.functions.calculateTime(hour)
            return `${ date } - ${ timeOfAppointment }`
        }
    }

    const deleteAccount = () => {
        Actions.SPAlert2({ 
            title: `Has decidido eliminar tu cuenta.`, 
            text: `Esta opción es irreversible y se eliminarán todos tus datos personales así como las citas que tengas previstas o que hayas realizado. ¿Estas seguro que quieres eliminar tu cuenta?`,
            setHeight: hp('4'),
            onPress1: () => deleteConfirmation() ,
            onPress2: () => cancel(),
            labelButton1: 'ELIMINAR',
            labelButton2: 'Cancelar',
        })
    }

    const deleteConfirmation = () => {
        setTimeout( () => {
            Actions.SPAlert2({ 
                title: `Has decidido eliminar tu cuenta.`, 
                text: `¿Seguro que quieres eliminar toda tu información personal y citas tanto pasadas como futuras?. Esta acción es IRREVERSIBLE`,
                setHeight: hp('4'),
                onPress1: () => ejecuteDeletion() ,
                onPress2: () => cancel(),
                labelButton1: 'ELIMINAR',
                labelButton2: 'Cancelar',
            })
        }, 300) 
    }

    const ejecuteDeletion = () => {
        //Delete user Slots
        firebase.firestore().collection('Slots')
        .where('customerEmail', '==', user.email)
        .get()
        .then( query => {
            if (query) {
                const usersId = []
                query.forEach( doc => {
                    usersId.push(doc.id)
                })
                usersId.forEach( doc => {
                    firebase.firestore().collection('Slots').doc(doc).delete()
                    .then( () => {
                        console.log('Slot ', doc, ' borrado')
                    })
                })
            }
        })
        .catch( error => {
            console.log('ErrorDeletingSlots: No se han borrado los slots de la cuenta a eliminar')
        })
        //Delete user
        firebase.firestore().collection('Users')
        .where('email', '==', user.email)
        .get()
        .then( query => {
            if (query) {
                const usersId = []
                query.forEach( doc => {
                    usersId.push(doc.id)
                })
                firebase.firestore().collection('Users').doc(usersId[0]).delete()
                .then( () => {
                    console.log('User ', usersId[0], ' borrado')
                })
            }
        })
        .catch( error => {
            console.log('ErrorDeletingSlots: No se han borrado los slots de la cuenta a eliminar')
        })
        //Delete User Parameters
        updateUserLogged({})
        updateUserToRemember({})
        updatePastAppointments([])
        updateUpcomingAppointments([])
        updateLastAppointment('')
        //Delete avatar
        firebase.storage().ref().child(`avatars/${ user.email }`)
        .delete()
        .then( () => {
            console.log('Avatar borrado')
        })
        .catch( error => {
            console.log('Avatar no borrado: ', error)
        })
        //Delete account
        const userToDelete = firebase.auth().currentUser
        userToDelete.delete()
        .then( () => {
            Actions.replace("WelcomeScreen")
            Actions.SPAlert({ 
                title: 'Aviso', 
                text: 'Has sido eliminado de nuestra comunidad.',
                setHeight: hp('4')
            })
        })
        .catch( error => {
            console.log('No se ha podido borrar el usuario de auth: ', error)
            Actions.replace("WelcomeScreen")
            Actions.SPAlert({ 
                title: 'Aviso', 
                text: 'Has sido eliminado de nuestra comunidad.',
                setHeight: hp('4')
            })
        })
    }

    const cancel = () => {
        console.log('Borrado cancelado')
    }

    if (user.email === 'invited') {
        const email = 'invited'
        Actions.replace("_Blog", { email })
        //Actions.replace("SignUpScreen")
        Actions.SPAlert({ 
            title: `Estas invitado`, 
            text: `Para acceder al perfil, debes registrarte`,
            setHeight: hp('4'),
        })
    }

    //Renders
    return(
        <View style={ styles.container }>
            <View style={ styles.header }>
                <View style={{ flexDirection: 'column' }}>
                    <TouchableOpacity
                        transparent
                        onPress={ logout }
                    >
                        <Exit 
                            marginLeft={ wp('10') }
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ width: wp('65') }} />
                <TouchableOpacity
                    transparent
                    onPress={ edit }
                >
                    <Edit />
                </TouchableOpacity>
            </View>
            <View style={ styles.avatarView }>
                <TouchableOpacity
                    transparent
                    onPress={ requestCameraPermission }
                    style={ styles.button }
                >
                    {
                        userAvatar
                        ?
                        <Image 
                            large 
                            source={ avatarLoaded ? { uri: avatarLoaded } : avatar }
                            style={ styles.thumbnail }
                        />
                        :
                        <View style={ styles.thumbnail }>
                            <AvatarDefault 
                                width={ hp('12') }
                                height={ hp('12') }
                                borderRadius={ hp('6') }
                            />
                        </View>
                    }
                    <Camera 
                        style={ styles.imageCamera } 
                        width={ Platform.OS === 'ios' ? wp('12') : wp('10') }
                        height={ Platform.OS === 'ios' ? hp('12') : hp('10') }
                    />
                </TouchableOpacity>
                <View style={{ width: Platform.OS === 'android' ? hp('6') : wp('5') }} />
                <View style={{ marginTop: hp('7')}}>
                    <Text style={ styles.textName }>{ user.name }</Text>
                    <Text style={ styles.textEmail }>{ user.email }</Text>
                </View>
            </View>
            <View style={{ height: hp('7')}} />
            <View style={{ flexDirection: 'row' }}>
                <View style={{ width: wp('65') }}>
                    <Text style={ styles.label }>Número de contacto</Text>
                    <Text style={ styles.title }>{ user.phone }</Text>
                </View>
                <TouchableOpacity
                    transparent
                    onPress={ goToFavourites }
                >
                    <Favourites 
                        marginTop={ hp('1') }
                        height={ Platform.OS === 'android' ? hp('5.4') : hp('3.7') }
                    />
                </TouchableOpacity>
                <View style={{ width: wp('5') }} />
                <TouchableOpacity
                    transparent
                    onPress={ deleteAccount }
                >
                    <DeleteAccount 
                        marginTop={ hp('1') }
                    />
                </TouchableOpacity>
            </View>
            <View style={ styles.line } />
            <View style={{ height: hp('2') }} />
            <View style={{ flexDirection: 'row' }}>
                <View style={{ width: wp('65') }}>
                    <Text style={ styles.label }>Centro de preferencia</Text>
                    <Text style={ styles.title }>{ user.preferredCenter }</Text>
                    <Text style={ styles.label }>{ user.phoneCenter }</Text>
                </View>
                <TouchableOpacity
                    transparent
                    onPress={ makeCall }
                >
                    <Phone 
                        marginTop={ hp('2') }
                    />
                </TouchableOpacity>
                <View style={{ width: wp('5') }} />
                <TouchableOpacity
                    transparent
                    onPress={ showAddress }
                >
                    <Address 
                        marginTop={ hp('2') }
                    />
                </TouchableOpacity>
            </View>
            <View style={ styles.line } />
            <View style={{ height: Platform.OS === 'android' ? hp('1') : hp('2') }} />
            <Text style={ styles.lastVisit }>Ultima visita</Text>
            <View style={{ height: Platform.OS === 'android' ? hp('0.1') : hp('2')}} />
            {
                lastAppointment
                ?
                <View>
                    <Text style={ styles.label }>Centro</Text>
                    <Text style={ styles.title }>{ lastAppointment.centerName }</Text>
                    <View style={{ height: hp('2')}} />
                    {/*
                    <Text style={ styles.label }>Categoria</Text>
                    <Text style={ styles.title }>{ lastAppointment.categoryName }</Text>
                    <View style={{ height: hp('2')}} />
                    */}
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ width: Dimensions.get('window').width / 2.2,}}>
                            <Text style={ styles.label }>Servicio</Text>
                            <Text style={ styles.title }>{ lastAppointment.serviceName }</Text>
                        </View>
                        <View style={{ width: wp('0') }} />
                        <View>
                            <Text style={ styles.label }>Terapeuta</Text>
                            <Text style={ styles.title }>{ lastAppointment.therapistName }</Text>
                        </View>
                    </View>
                    <View style={{ height: hp('2')}} />
                    <Text style={ styles.label }>Fecha</Text>
                    <Text style={ styles.title }>{ showLastAppointmentDate(lastAppointment.serviceDate, lastAppointment.serviceHour) }</Text>
                </View>
                :
                <Text style={ styles.title }>No ha realizado aún ninguna visita</Text>
            }
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9',
    },
    header: {
        flexDirection: 'row',
        marginTop: hp('7'),
    },
    textExit: {
        color: '#CBAC77',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('2'),
        marginLeft: wp('10'),
    },
    textEdit: {
        color: '#CBAC77',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('2'),
    },
    avatarView: {
        flexDirection: 'row',
        marginTop: hp('4'),
        marginLeft: wp('10'),
        width: wp('50'),
    },
    thumbnail: {
        width: hp('14'),
        height: hp('14'),
        borderRadius: hp('7'),
        resizeMode: 'cover',
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageCamera:{
        position: 'absolute',
        top: Platform.OS === 'android' ? hp('3.8') : hp('5'),
        left: Platform.OS === 'android' ? wp('14') : wp('17.5'),
        marginLeft: wp('3'),
        marginBottom: hp('3'),
    },
    textName: {
        color: '#222427',
        fontFamily: 'Helvetica Neue Bold',
        fontSize: hp('1.8'),
        marginTop: hp('1'),
    },
    textEmail: {
        color: '#707070',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.6'),
        marginTop: hp('1'),
    },
    lastVisit: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.5'),
        marginLeft: wp('10'),
        paddingLeft: wp('2.5'),
    },
    title: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
        color: '#222427',
        marginLeft: wp('10'),
        marginTop: hp('1'),
    },
    label: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.4'),
        color: '#707070',
        marginLeft: wp('10'),
    },
    line: {
        height: hp('3'), 
        borderBottomColor: '#CBAC77',
        borderBottomWidth: hp('0.1'), 
        width: Dimensions.get('window').width - wp('20'),
        marginLeft: wp('10'),
    },
})

export default Profile