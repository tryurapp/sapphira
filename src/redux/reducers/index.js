import login from './login'
import appointments from './appointments'
import blog from './blog'

export {
    login,
    appointments,
    blog
}