import * as types from '../types';

const initialState = {
    count: 0
};

export default function countsReducer(state = initialState, action={})  {
    switch(action.type) {
        case types.COUNTER_CHANGE:
            return {
                ...state,
                count: action.count
        };
        default:
            return state;
    }
}

