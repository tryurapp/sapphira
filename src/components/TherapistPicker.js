//React imports
import React, { useState } from 'react'
//React native imports
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
//Components imports
import TUA_Button from './TUA_Button'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React native router flux imports
import { Actions } from 'react-native-router-flux'
//Picker imports
import { Picker } from '@react-native-picker/picker'
//Redux imports
import { useDispatch } from 'react-redux'
import * as SPActions from '../redux/actions'



const TherapistPicker = props => {

    //Connect Redux for functional component
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updatePickerTherapist = (pickerTherapist) => {
        dispatch(SPActions.appointments.updatePickerTherapist(pickerTherapist))
    }

    //State
    const [selectedTherapist, setSelectedTherapist] = useState()
    const [selectedTherapistIndex, setSelectedTherapistIndex] = useState('')

    //Constants
    const { therapists } = props

    //Functions
    const itemSelected = () => {
        if (selectedTherapistIndex !== '0' && selectedTherapistIndex) {
            let id = ''
            therapists.forEach( element => {
                if (element.name === selectedTherapist) {
                    id = element.id
                }
            });
            updatePickerTherapist({
                'name': selectedTherapist,
                'id': id
            })
        }
        Actions.pop( { refresh: {} } )
    }
    
    //Renders
    return(
        <View style={ styles.container }>
            <View style={{ height: hp('1.5') }} />
            <View style={ styles.viewCancel }>
                <TouchableOpacity
                    transparent
                    onPress={ () => Actions.pop({ refresh: {} }) }
                >
                    <Text style={ styles.textCancel }>Cancelar</Text>
                </TouchableOpacity>
            </View>
            <Picker
                selectedValue= { selectedTherapist }
                onValueChange={ (itemValue, itemIndex) =>  {
                        setSelectedTherapist(itemValue)
                        setSelectedTherapistIndex(itemIndex.toString()) 
                    }
                }  
            >
                   { 
                    Object.keys(therapists).map( key => {
                        return (
                            <Picker.Item 
                                label={ therapists[key].name } 
                                value={ therapists[key].name }
                                key={ key } 
                            />
                        )
                    })
                   }
            </Picker>
            <View style={ styles.button }>
                <TUA_Button
                    bgColor={ '#CBAC77' }
                    label={ 'Listo' }
                    labelColor={ '#FFFFFF'}
                    setWidth={ wp('80') }
                    onPress={ itemSelected }
                    fontSize={ hp('1.7') }
                    setActivity={ false }
                    setHeight={ hp('5.5') }
                    buttonStyle={{ borderRadius: hp('0.5'),}}
                />
            </View>
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#E2E2E2',
        position: 'absolute',
        top: Dimensions.get('window').height / 2,
        bottom: 0,
        left: 0,
        right: 0,
        borderRadius: wp('4')
    },
    button: {
        alignItems: 'center',
        marginTop: hp('3'),
    },
    viewCancel: {
        alignItems: 'flex-end',
        marginRight: wp('10'),
    },
    textCancel: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
        color: '#CBAC77',
    },
})

export default TherapistPicker