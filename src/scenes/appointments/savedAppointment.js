//React imports
import React from 'react'
//React native imports
import { StyleSheet, Text, View, Image, Platform, ScrollView } from 'react-native'
//Components import
import TUA_Button from '../../components/TUA_Button'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
import moment from 'moment'
//Redux imports
import { useSelector } from 'react-redux'
//Utils imports
import * as Utils from '../../utils/'



const SavedAppointment = props => {

    //Constants
    const { appointment, type } = props

    //Functions
    const centersData = useSelector(
        state => state.blog.centers
    )
    const dateOfAppointment = moment(appointment.serviceDate._seconds * 1000).format('DD/MM/YYYY')
    const dateApp = moment(appointment.serviceDate._seconds * 1000).format('MM, DD')
    const monthApp = dateApp.substring(0, 2)
    const dayApp = dateApp.substring(4, 6)
    const monthDay =`${ dayApp } de ${ Utils.functions.calculateMonth(monthApp)}`
    const timeOfAppointment = Utils.functions.calculateTime(appointment.serviceHour)

    const updateAppointment = () => {
        let centerEmail = ''
        centersData.forEach( center => {
            if (center.center.centerName === appointment.centerName) {
                centerEmail = center.center.email
            }
        })
        if (type === 'sendEmail') {
            //Enviar email
            fetch(`https://us-central1-sapphira-c8887.cloudfunctions.net/sendMail?userEmail=${ appointment.customerEmail }&centerEmail=${ centerEmail }&userName=${appointment.customerName}&userPhone=${ appointment.customerPhone }&service=${ appointment.serviceName }&therapist=${ appointment.therapistName }&serviceHour=${ Utils.functions.calculateTime(appointment.serviceHour) }&monthDay=${ monthDay }&center=${ appointment.centerName }&confirmCode=${ appointment.confirmationCode }`)
            .then( () => {
                Actions.replace("_Appointments")
            } )
            .catch( error => {
                //console.error(error)
                Actions.SPAlert({ 
                    title: `Error.`, 
                    text: 'Ha surgido un error al enviar el email de confirmación de la cita.',
                    setHeight: hp('4')
                })
                Actions.replace("_Appointments")
            })
        }
        Actions.replace("_Appointments")
    }

    //Render component
    return (
        <ScrollView>
        <View style={ styles.container }>
            <Image 
                source={ require('../../resources/appointments/appointmentsPhoto.png') }
                style={ Platform.OS === 'android' ? styles.imageAndroid : styles.image }
                resizeMode='cover'
            />
            <View style={{ height: hp('4') }} />
            <Text style={ styles.text }>Confirmado</Text>
            <View style={{ height: Platform.OS === 'android' ? hp('0.5') : hp('3') }} />
            <Text style={styles.title }>Hemos agendado su cita:</Text>
            <View style={{ height: hp('3') }} />
            <Text style={ styles.label}>Centro</Text>
            <Text style={ styles.title }>{ appointment.centerName }</Text>
            <View style={{ height: hp('2') }} />
            <Text style={ styles.label}>Categoría</Text>
            <Text style={ styles.title }>{ appointment.categoryName }</Text>
            <View style={{ height: hp('2') }} />
            <View style={{ flexDirection: 'row'}}>
                <View style={{ width: wp('45') }}>
                    <Text style={ styles.label}>Servicio</Text>
                    <Text style={ styles.title }>{ appointment.serviceName }</Text>
                </View>
                <View>
                    <Text style={ styles.label}>Terapeuta</Text>
                    <Text style={ styles.title }>{ appointment.therapistName }</Text>
                </View>
            </View>
            <View style={{ height: hp('2') }} />
            <View style={{ flexDirection: 'row'}}>
                <View style={{ width: wp('45') }}>
                    <Text style={ styles.label}>Fecha</Text>
                    <Text style={ styles.title }>{ dateOfAppointment }</Text>
                </View>
                <View>
                    <Text style={ styles.label}>Hora</Text>
                    <Text style={ styles.title }>{ timeOfAppointment }</Text>
                </View>
            </View>
            <View style={{ height: hp('2') }} />
            <Text style={ styles.title }>Su código de confirmación es:</Text>
            <View style={{ height: hp('1') }} />
            <View style={ styles.viewConfirm }>
                <Text style={ styles.confirm }>{ appointment.confirmationCode }</Text>
            </View>
            <View style={{ height: hp('2') }} />
            <Text style={ styles.label }>Muestre este código en la recepción de su centro.</Text>
            
            <View style={{ height: hp('3') }} />
            
            <TUA_Button
                bgColor={ '#CBAC77' }
                label={ 'Listo' }
                labelColor={ '#FFFFFF' }
                setWidth={ wp('80') }
                onPress={ () => updateAppointment() }
                fontSize={ hp('1.7') }
                setActivity={ false }
                setHeight={ hp('5.5') }
                buttonStyle={{ marginLeft: wp('10'), borderRadius: hp('0.5'),}}
            />
        </View>
        </ScrollView>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9',
    },
    text: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.5'),
        marginLeft: wp('10'),
        paddingLeft: wp('2.5'),
    },
    title: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.8'),
        color: '#222427',
        marginLeft: wp('10'),
        marginTop: hp('1'),
    },
    label: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.4'),
        color: '#707070',
        marginLeft: wp('10'),
    },
    confirm: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('3.5'),
        color: '#222427',
        marginTop: hp('1'),
    },
    viewConfirm: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageAndroid: {
        height: hp('20'),
    },
  })

  export default SavedAppointment