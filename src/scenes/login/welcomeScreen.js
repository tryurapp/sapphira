//React imports
import React, { useEffect, useState } from 'react'
//React native imports
import { Text, StyleSheet, View, Image, TouchableOpacity, ActivityIndicator, Dimensions, Platform } from 'react-native'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
//Components imports
import TUA_Button from '../../components/TUA_Button'
//Firebase imports
import firebase from '@react-native-firebase/app'
import auth  from '@react-native-firebase/auth'
import { GoogleSignin } from 
'@react-native-google-signin/google-signin'
GoogleSignin.configure({
    scopes: ['email', 'profile'],
    webClientId: '1042604533055-itsq0b64kbc6tbtvh4ao3pulbgkhtqo7.apps.googleusercontent.com',
    offlineAccess: false,
})
import { appleAuth } from '@invertase/react-native-apple-authentication'
import { LoginManager, AccessToken } from 'react-native-fbsdk-next'
//Redux imports
import { useDispatch } from 'react-redux'
import * as SPActions from '../../redux/actions'
//Images import 
import IconFacebook from '../../resources/login/iconFacebook'
import IconApple from '../../resources/login/iconApple'
import IconGoogle from '../../resources/login/iconGoogle'
import * as Utils from '../../utils'



const WelcomeScreen = () => {

    //Connect Redux for functional component
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updateUserLoggedData = (userLogged) => {
        dispatch(SPActions.login.updateUserLoggedData(userLogged))
    }
    const updateUserLogged = (userLogged) => {
        dispatch(SPActions.login.updateUserLogged(userLogged))
    }

    //State
    const [activity, setActivity] = useState(false)
    const [typeLogin, setTypeLogin] = useState('')

    //useEffect
    useEffect( () => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged)
        return subscriber
    }, [])

    //Functions
    function onAuthStateChanged(user) {
        if (user) {
            setActivity(false)
            const userToUpdate = {
                email: user._user.email
            }
            updateUserLoggedData(userToUpdate)
            const email = user._user.email
            if (email) Actions.replace("_Blog", { email })
        }
    }

    const register = () => {
        Actions.replace("SignUpScreen")
    }

    const login = () => {
        Actions.replace("SignInScreen")
    }

    const invited = () => {
        const userToUpdate = {
            name: 'invited',
            email: 'invited'
        }
        const email = 'invited'
        updateUserLogged(userToUpdate)
        Actions.replace("_Blog", { email })
        Actions.SPAlert({ 
            title: `Estas invitado`, 
            text: 'Puedes entrar pero tendrás muchas opciones inaccesibles. Para acceder a ellas deberias registrarte.',
            setHeight: hp('4')
        })
    }

    const googleLogin = async () => {
        setActivity(true)
        const currentUser = await GoogleSignin.signIn()
        if (!currentUser.idToken) {
            console.log('Apple Sign-In failed - no identify token returned')
        }
        const googleCredential = auth.GoogleAuthProvider.credential(currentUser.idToken)
        //Esta el user ya creado?
        firebase.firestore().collection('Users')
        .where('email', '==', currentUser.user.email)
        .get()
        //Si si, recojo su información
        .then( query => {
            setActivity(false)
            const userId = []
            query.forEach( doc => {
                userId.push(doc.id)
            })
            if (userId[0]) {
                return auth().signInWithCredential(googleCredential)
            } else {
                Actions.SPAlert({ 
                    title: 'Atención', 
                    text: `El usuario ${ currentUser.user.email } no está registrado. Para poder acceder a la App, primero debe registrarse.`,
                    setHeight: hp('4')
                }) 
            }
            
        })
        //Si no, aviso de que debe primero registrarse
        .catch( error => {
            console.log('Error in login with Google. ', ErrorEvent)
        })
    }

    const appleLogin = async () => {
        setActivity(true)
        const appleAuthRequestResponse = await appleAuth.performRequest({
            requestedOperation: appleAuth.Operation.LOGIN,
            requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
        })
        setActivity(false)
        if (!appleAuthRequestResponse.identityToken) {
            console.log('Apple Sign-In failed - no identify token returned')
        }
        const { identityToken, nonce } = appleAuthRequestResponse
        const appleCredential = auth.AppleAuthProvider.credential(identityToken, nonce)
        setTypeLogin('apple')
        return auth().signInWithCredential(appleCredential)
    }

    const facebookLogin = async () => {
        console.log('FacebookLogin')
        
        const result = await LoginManager.logInWithPermissions(['public_profile', 'email'])
        if (result.isCancelled) {
            throw 'User cancelled the login access'
        }
        const data = await AccessToken.getCurrentAccessToken()
        setActivity(false)
        if (!data) {
            throw 'Something went wrong obtaining access token'
        }
        console.log('Data: ', data)
        const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken)
        setTypeLogin('facebook')
        return auth().signInWithCredential(facebookCredential)
    }

    //Renders
    return(
        <View style={ styles.textStyle }>
            <View style={{ height: hp('17.18') }} />
            <Text style={ styles.text }>Bienvenido a la experiencia</Text>
            <View style={{ height: Platform.OS === 'ios' ? hp('5.5') : hp('4') }} />
            <Image
                source={ require('../../resources/Logo.png')}
                resizeMode='contain'
            />
            <View style={{ height: hp('7.46') }} />
            <TUA_Button
                bgColor={ '#FFFFFF' }
                label={ 'Registrarme' }
                labelColor={ '#CBAC77'}
                setWidth={ Dimensions.get('window').width - wp('14.96') }
                onPress={ register }
                buttonStyle={{
                    borderWidth: hp('0.1'),
                    borderRadius: hp('0.55'),
                }}
                fontSize={ hp('1.62') }
                setActivity={ false }
                setHeight={ hp('5.84') }
            />
            <View style={{ height: hp('2.71') }} />
            <TUA_Button
                bgColor={ '#FFFFFF' }
                label={ 'Ingresar como invitado' }
                labelColor={ '#CBAC77'}
                setWidth={ Dimensions.get('window').width - wp('14.96') }
                onPress={ invited }
                buttonStyle={{
                    borderWidth: hp('0.1'),
                    borderRadius: hp('0.55'),
                }}
                fontSize={ hp('1.62') }
                setActivity={ false }
                setHeight={ hp('5.84') }
            />
            <View style={{ height: hp('2.71') }} />
            <TUA_Button
                bgColor={ '#CBAC77' }
                label={ 'Iniciar sesión' }
                labelColor={ '#FFFFFF'}
                setWidth={ Dimensions.get('window').width - wp('14.96') }
                onPress={ login }
                buttonStyle={{
                    borderRadius: hp('0.55'),
                }}
                fontSize={ hp('1.62') }
                setActivity={ false }
                setHeight={ hp('5.84') }
            />
            <View style={{ height: hp('2.16') }} />
            {
                activity
                ?
                    <ActivityIndicator 
                        size="large" 
                        color='#CBAC77'
                    />
                :
                null
            }
            <View style={{ height: hp('2.16') }} />
            <View style={{ flexDirection: 'row' }}>
                { /* 
                <TouchableOpacity
                    transparent
                    onPress={ appleLogin }
                >
                    <Image
                        source={ require('../../resources/login/appleIcon.png')}
                        height={ hp('5.5') }
                        width={ hp('5.5') }
                    />
                </TouchableOpacity>
                <View style={{ width: hp('6.8') }} />
                               
                <TouchableOpacity
                    transparent
                    onPress={ facebookLogin }
                >
                    <IconFacebook 
                        height={ hp('5.5') }
                        width={ hp('5.5') }
                    />
                </TouchableOpacity>
                <View style={{ width: hp('6.8') }} />
                 */}
                {
                    Platform.OS === 'android' 
                    ?
                    <TouchableOpacity
                        transparent
                        onPress={ googleLogin }
                    >
                        <IconGoogle 
                            height={ hp('5.5') }
                            width={ hp('5.5') }
                        />
                    </TouchableOpacity>
                    :
                    null
                }
            </View>
            <View style={{ height: hp('20') }} />
            <Text 
                style={{
                color: '#CBAC77',
                fontFamily: 'Helvetica Neue',
                fontSize: hp('1.5'),
                marginLeft: wp('60'),
            }}
            >V.{ Platform.OS === 'ios' ? Utils.constants.IOS_VERSION : Utils.constants.ANDROID_VERSION }</Text>
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    textStyle: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        alignItems: 'center',
    },
    text: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.75'),
    },
})

export default WelcomeScreen