//React imports
import React, { Component } from 'react'
//React native imports
import { View, Text, StyleSheet, TextInput, Dimensions, Platform, Image, Button } from 'react-native'
//Native base imports
//import { Button, Left } from 'native-base'
//PropTypes imorts
import PropTypes from 'prop-types'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//Images imports
import InputError from '../resources/login/inputError'
import InputOK from '../resources/login/inputOK'
import NotSeePass from '../resources/login/notSeePass'
import SeePass from '../resources/login/seePass'

export default class TUA_Input extends Component {


    static propTypes = {
        labelStyle: PropTypes.object,
        inputStyle: PropTypes.object,
        errorStyle: PropTypes.object,
        label: PropTypes.string,
        value: PropTypes.string,
        error: PropTypes.string,
        labelSize: PropTypes.number,
        valueSize: PropTypes.number,
        errorSize: PropTypes.number,
        placeholder: PropTypes.string,
        onChangeText: PropTypes.func,
        onChange: PropTypes.func,
        isPassword: PropTypes.bool,
        keyboardType: PropTypes.string,
        onSubmitEditing: PropTypes.func,
        multiline: PropTypes.bool,
        isPasswordIcon: PropTypes.bool,
        seePassword: PropTypes.bool,
        validationField: PropTypes.string,
        width: PropTypes.number,
    }

    /*
    With "isPasswordIcon", we see or not the seePass icon. If "isPasswordIcon=true", with "seePassword" we define if see or not the password. If "seePassword=true", we see the password.

    With "validationField" we say if not show the icon ("validationField=0"), if show the correct icon ("validationField=1") or show the incorrect icon ("validationField=2")
    */

    static defaultProps = {
        labelStyle: {},
        inputStyle: {},
        errorStyle: {},
        labelSize: Platform.OS === 'android' ? hp('1.3') : hp('1.5'),
        valueSize: Platform.OS === 'android' ? hp('1.8') : hp('1.9'),
        errorSize: Platform.OS === 'android' ? hp('1.3') : hp('1.5'),
        placeholder: 'Text input',
        onChangeText: () => {},
        onSubmitEditing: () => {},
        onChange: () => {},
        isPassword: false,
        keyboardType: 'default',
        multiline: false,
        isPasswordIcon: false,
        seePassword: false,
        validationField: '0',
    }

    constructor(props) {
        super(props)
        this.state = {
            focus: false,
            seePasswordStatus: this.props.seePassword,
        }
    }

    changeFocus() {
        if (!this.props.value || this.props.value == '') {
            this.setState({ focus: !this.state.focus })
        }
    }

    changeSeePassword() {
        this.setState({
            seePasswordStatus: !this.state.seePasswordStatus,
        })
    }

    //RENDER
    render() {
        const { labelStyle, label, value, onChangeText, inputStyle, error, errorStyle, keyboardType, onChange, onSubmitEditing, multiline, placeholderTextColor, validationField, isPasswordIcon, width } = this.props
        const { seePasswordStatus } = this.state
        let { placeholder } = this.props
        let { focus } = this.state
        let placeText = false
        if (focus) {
            if (label) {
                placeText = true
                placeholder = ''
            } 
        } else {
            placeholder = label
        }
        
        return (
            <View style={ styles.container }>
                {
                    placeText ? 
                        <Text 
                            style= { [styles.label, labelStyle, { fontSize: this.props.labelSize }] }
                        >
                            { label }
                        </Text> : null
                }
                <View style={{ flexDirection: 'row' }}>
                    { /* <Left> */ }
                        <TextInput
                            value={ value }
                            onChangeText={ (v) => onChangeText(v) }
                            onFocus={ () => this.changeFocus() }
                            onChange={ () => onChange() }
                            onSubmitEditing={ ()=> onSubmitEditing()}
                            placeholder={ placeholder }
                            placeholderTextColor={ 'grey' }
                            autoCapitalize='none'
                            style={ 
                                [styles.input, inputStyle, { fontSize: this.props.valueSize, width: width }] }
                            underlineColorAndroid={ 'transparent' }
                            secureTextEntry={ !isPasswordIcon ?  false : !seePasswordStatus }
                            keyboardType={ keyboardType }
                            multiline={ multiline }
                        />
                    { /* </Left> */}
                    {
                        validationField === '0' 
                        ? 
                            <Button transparent title=''>
                                <Image
                                    resizeMode='contain'
                                />
                            </Button>
                        :
                            validationField === '1' ?
                            <Button transparent title=''>
                                <InputOK />
                            </Button>
                            :
                            <Button transparent title=''>
                                <InputError />
                            </Button>
                    }
                    {
                        (!isPasswordIcon || value === '' || !value) ? null :
                            seePasswordStatus ?
                            <Button transparent
                                style={{ marginLeft: wp('3') }} 
                                onPress={ () => this.changeSeePassword() }
                                title=''
                            >
                                <SeePass />
                            </Button>
                            :
                            <Button transparent 
                                style={{ marginLeft: wp('3') }}
                                onPress={ () => this.changeSeePassword() }
                                title=''
                            >
                                <NotSeePass />
                            </Button>
                    }
                </View>
                { /* <View style={ styles.inputBottomLine }/> */ }
                { error ? 
                    <Text 
                        style={ [styles.error, errorStyle, { fontSize: this.props.errorSize }] }
                    >
                        { error }
                    </Text> 
                    : null 
                }
            </View>
        )
    }
}



//ESTILOS
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width - wp('12'),
        height: hp('5'),
    },
    error: {
        color: 'red',
        textAlign: 'right',
        fontFamily: 'Helvetica Neue',
    },
    input: {
        color: '#707070',
        width: Dimensions.get('window').width - wp('25'),
        fontFamily: 'Helvetica Neue',
        borderWidth: hp('0.05'),
        borderRadius: hp('0.5'),
        borderColor: 'grey',
        paddingLeft: wp('2'),
        paddingTop: wp('2'),
        paddingBottom: hp('1'),
        marginTop: hp('0.7'),
    },
    label: {
        color: '#707070',
        fontFamily: 'Helvetica Neue',
        marginTop: hp('0.7'),
    },
    inputBottomLine: {
        width: Dimensions.get('window').width - wp('12'),
        borderBottomColor: '#707070',
        borderBottomWidth: wp('0.3'),
    }
})