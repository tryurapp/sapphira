//React imports
import React, { Component } from 'react'
//React native imports
import { View, StyleSheet, Dimensions, Platform, TouchableOpacity } from 'react-native'
//PropTypes imorts
import PropTypes from 'prop-types'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//Images imports
import NotSeePass from '../resources/login/notSeePass'
import SeePass from '../resources/login/seePass'
//React native elements input
import { Input } from 'react-native-elements'


export default class SAP_Input extends Component {

    //PropTypes
    static propTypes = {
        labelStyle: PropTypes.object,
        inputStyle: PropTypes.object,
        errorStyle: PropTypes.object,
        label: PropTypes.string,
        value: PropTypes.string,
        error: PropTypes.string,
        labelSize: PropTypes.number,
        valueSize: PropTypes.number,
        errorSize: PropTypes.number,
        placeholder: PropTypes.string,
        onChangeText: PropTypes.func,
        onChange: PropTypes.func,
        isPassword: PropTypes.bool,
        keyboardType: PropTypes.string,
        onSubmitEditing: PropTypes.func,
        multiline: PropTypes.bool,
        isPassword: PropTypes.bool,
        validationField: PropTypes.string,
        width: PropTypes.number,
    }

    //State
    static defaultProps = {
        labelStyle: {},
        inputStyle: {},
        errorStyle: {},
        labelSize: Platform.OS === 'android' ? hp('1.3') : hp('1.2'),
        valueSize: Platform.OS === 'android' ? hp('1.8') : hp('1.5'),
        errorSize: Platform.OS === 'android' ? hp('1.3') : hp('1.2'),
        placeholder: 'Text input',
        onChangeText: () => {},
        onSubmitEditing: () => {},
        onChange: () => {},
        isPassword: false,
        keyboardType: 'default',
        multiline: false,
        isPassword: false,
        validationField: '0',
    }

    //Constructor
    constructor(props) {
        super(props)
        this.state = {
            focus: false,
            seePasswordStatus: this.props.isPassword,
        }
    }

    //Functions
    changeFocus() {
        if (!this.props.value || this.props.value == '') {
            this.setState({ focus: !this.state.focus })
        }
    }

    changeSeePassword() {
        this.setState({
            seePasswordStatus: !this.state.seePasswordStatus,
        })
    }

    //REenders
    render() {
        const { labelStyle, label, value, onChangeText, inputStyle, error, errorStyle, keyboardType, onChange, onSubmitEditing, multiline, width, isPassword, errorSize, labelSize, valueSize } = this.props
        const { seePasswordStatus } = this.state
        let { placeholder } = this.props
        let { focus } = this.state
        let placeText = false
        if (focus) {
            if (label) {
                placeText = true
                placeholder = ''
            } 
        } else {
            placeholder = label
        }
        
        return (
            <View style={ [styles.container, { height: this.state.focus ? hp('7') : error ? hp('9') : hp('5') }] }>
                <Input
                    value={ value }
                    onChangeText={ (v) => onChangeText(v) }
                    onFocus={ () => this.changeFocus() }
                    onChange={ () => onChange() }
                    onSubmitEditing={ ()=> onSubmitEditing()}
                    placeholder={ placeholder }
                    placeholderTextColor={ 'grey' }
                    autoCapitalize='none'
                    inputContainerStyle={ 
                        [styles.input, inputStyle, { fontSize: valueSize, width: width }] }
                    underlineColorAndroid={ 'transparent' }
                    secureTextEntry={ seePasswordStatus ?  true : false }
                    keyboardType={ keyboardType }
                    multiline={ multiline }
                    rightIcon={
                        isPassword ? 
                        <TouchableOpacity
                            onPress={ () => this.setState({ seePasswordStatus: !this.state.seePasswordStatus })}
                        >
                            { seePasswordStatus 
                            ? <SeePass /> : <NotSeePass /> }
                        </TouchableOpacity>
                        : null
                    }
                    errorMessage={ error }
                    errorStyle={ styles.error }
                    label={ focus ? label : '' }
                    labelStyle={ [styles.label, { fontSize: labelSize }, labelStyle] }
                />
            </View>
        )
    }
}

//Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: Dimensions.get('window').width - wp('12'),
    },
    error: {
        color: 'red',
        textAlign: 'right',
        fontFamily: 'Helvetica Neue',
    },
    input: {
        color: '#707070',
        width: Dimensions.get('window').width - wp('20'),
        fontFamily: 'Helvetica Neue',
        borderWidth: hp('0.05'),
        borderRadius: hp('0.5'),
        borderColor: '#CBAC771C',
        paddingLeft: wp('2'),
        paddingTop: wp('2'),
        paddingBottom: hp('1'),
        marginTop: hp('0.7'),
        height: hp('5'),
    },
    label: {
        color: '#707070',
        fontFamily: 'Helvetica Neue',
        marginTop: hp('0.3'),
    },
    inputBottomLine: {
    }
})