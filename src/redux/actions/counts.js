import * as types from '../types';

export const changeCount = (count) => {
    return {
        type: types.COUNTER_CHANGE,
        count
    }
}
