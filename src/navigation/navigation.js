//React imports
import React from 'react';
//React Native imports
import { StyleSheet } from 'react-native';
//Components imports
import WelcomeScreen from '../scenes/login/welcomeScreen'
import SignUpScreen from '../scenes/login/signUpScreen'
import SignInScreen from '../scenes/login/signInScreen'
import Blog from '../scenes/blog/blog'
import Appointments from '../scenes/appointments/appointments'
import Profile from '../scenes/profile/profile'
import TabIconAppointments from '../components/TabIconAppointments.js'
import TabIconBlog from '../components/TabIconBlog'
import TabIconProfile from '../components/TabIconProfile'
import SPAlert from '../components/SPAlert'
import SPAlert2 from '../components/SPAlert2'
import UserForm from '../components/UserForm'
import NewAppointment from '../scenes/appointments/newAppointment'
import SavedAppointment from '../scenes/appointments/savedAppointment'
import CenterPicker from '../components/CenterPicker'
import CategoryPicker from '../components/CategoryPicker'
import ServicePicker from '../components/ServicePicker'
import TherapistPicker from '../components/TherapistPicker'
import Featured from '../scenes/blog/featured'
import PostDetail from '../scenes/blog/postDetail'
import Favourites from '../scenes/blog/favourites'
import Store from '../components/Store'
//React Native Router Flux imports
import { Router, Scene, Lightbox, Actions } from 'react-native-router-flux'


const Navigation = (props) => {

    //Functions
    const goTabSelection = (index) => {
        //const email = user.email
        const selection = index.navigation.state.key
        Actions.jump(`_${ selection }`)
    }

    //Renders
    //panHandlers prevent gestures in scenes
    return(
        <Router panHandlers={ null }>
            <Lightbox>
                <Scene key='root' hideNavBar >
                    <Scene key='tabBar'>
                        <Scene
                            key="main" 
                            tabs={ true } 
                            lazy={ true }
                            hideNavBar 
                            showLabel={ false }
                            default='Blog'
                            tabBarStyle={{ backgroundColor: '#E2E2E2' }}
                            tabBarOnPress={ (index) => {
                                goTabSelection(index)
                            }}
                        >
                            <Scene
                                hideNavBar
                                key={ 'Blog' }
                                component={ Blog }
                                icon={ TabIconBlog }
                            />
                            <Scene
                                hideNavBar
                                key={ 'Appointments' }
                                component={ Appointments }
                                icon={ TabIconAppointments }
                            />
                            <Scene
                                hideNavBar
                                key={ 'Profile' }
                                component={ Profile }
                                icon={ TabIconProfile }
                            />   
                        </Scene>
                        <Scene
                            hideNavBar
                            key={ 'WelcomeScreen' }
                            component={ WelcomeScreen }
                            initial={ true }
                        />
                        <Scene
                            hideNavBar
                            key={ 'SignUpScreen' }
                            component={ SignUpScreen }
                        />
                        <Scene
                            hideNavBar
                            key={ 'SignInScreen' }
                            component={ SignInScreen }
                        />
                        <Scene
                            hideNavBar
                            key={ 'NewAppointment' }
                            component={ NewAppointment }
                        />
                        <Scene
                            hideNavBar
                            key={ 'SavedAppointment' }
                            component={ SavedAppointment }
                        />
                        <Scene
                            hideNavBar
                            key={ 'Featured' }
                            component={ Featured }
                        />
                        <Scene
                            hideNavBar
                            key={ 'Favourites' }
                            component={ Favourites }
                        />
                        <Scene
                            hideNavBar
                            key={ 'PostDetail' }
                            component={ PostDetail }
                        />
                        <Scene
                            hideNavBar
                            key={ 'Store' }
                            component={ Store }
                        />
                    </Scene>
                </Scene>
                <Scene
                    hideNavBar
                    key={ 'SPAlert' }
                    component={ SPAlert }
                />
                <Scene
                    hideNavBar
                    key={ 'SPAlert2' }
                    component={ SPAlert2 }
                />
                <Scene
                    hideNavBar
                    key={ 'CenterPicker' }
                    component={ CenterPicker }
                />
                <Scene
                    hideNavBar
                    key={ 'CategoryPicker' }
                    component={ CategoryPicker }
                />
                <Scene
                    hideNavBar
                    key={ 'ServicePicker' }
                    component={ ServicePicker }
                />
                <Scene
                    hideNavBar
                    key={ 'TherapistPicker' }
                    component={ TherapistPicker }
                />
                <Scene
                    hideNavBar
                    key={ 'UserForm' }
                    component={ UserForm }
                />
            </Lightbox>
        </Router>
    )
}

//Styles
const styles = StyleSheet.create({  
})

export default Navigation