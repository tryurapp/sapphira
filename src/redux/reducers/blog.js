import * as types from '../types';

const initialState = {
    centers: {},
    wpFeatured: [],
    wpPosts: [],
    version: '',
};

export default function blogReducer(state = initialState, action={})  {
    switch(action.type) {
        case types.BLOG_UPDATE_CENTERS:
            return {
                ...state,
                centers: action.centers
        }
        case types.BLOG_UPDATE_WP_FEATURED:
            return {
                ...state,
                wpFeatured: action.wpFeatured
        }
        case types.BLOG_UPDATE_WP_POSTS:
            return {
                ...state,
                wpPosts: action.wpPosts
        }
        case types.UPDATE_VERSION:
            return {
                ...state,
                version: action.version
        }
        default:
            return state;
    }
}