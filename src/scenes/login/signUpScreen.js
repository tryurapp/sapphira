//React imports
import React, { useState } from 'react'
//React native imports
import { Text, StyleSheet, View, Image, TouchableOpacity, Dimensions, Platform } from 'react-native'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
//Components imports
import TUA_Button from '../../components/TUA_Button'
import SapphiraInput from '../../components/SapphiraInput'
//Validate imports
import validator from 'email-validator'
//Keyboard imports
import { KeyboardAvoidingScrollView } from 'react-native-keyboard-avoiding-scroll-view'
//Firebase imports
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/firestore'
import auth from '@react-native-firebase/auth'
//Redux imports
import { useDispatch } from 'react-redux'
import * as SPActions from '../../redux/actions'
//ImageImports
import Back from '../../resources/appointments/backDateHours'



const SignUpScreen = () => {

    //Connect Redux for functional component
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updateUserLogged = (userLogged) => {
        dispatch(SPActions.login.updateUserLogged(userLogged))
    }

    //State
    const [name, setName] = useState('')
    const [nameError, setNameError] = useState('')
    const [email, setEmail] = useState('')
    const [emailError, setEmailError] = useState('')
    const [password, setPassword] = useState('')
    const [passwordError, setPasswordError] = useState('')
    const [repeatPassword, setRepeatPassword] = useState('')
    const [repeatPasswordError, setRepeatPasswordError] = useState('')
    const [warning, setWarning] = useState('')
    const [activity, setActivity] = useState(false)

    //Functions
    const validateForm = () => {
        let valid = true
        let errors = {}
        //Validation of name
        if (!name)  {
            errors.name = 'Introduce un nombre'
            valid = false
        }
        //Validation of email
        if (!email)  {
            errors.email = 'Introduce un email'
            valid = false
        }
        //Validation of password
        if (!password)  {
            errors.password = 'Introduce una contraseña'
            valid = false
        } else if (password.length < 6){
            errors.password = 'La contraseña debe tener al menos 6 caracteres'
            valid = false
        }
        //Validation of repeatPassword
        if (!repeatPassword)  {
            errors.repeatPassword = 'Introduce una contraseña'
            valid = false
        } else if (repeatPassword.length < 6){
            errors.repeatPassword = 'La contraseña debe tener al menos 6 caracteres'
            valid = false
        } else if (password !== repeatPassword) {
            errors.repeatPassword = 'Las contraseñas deben coincidir'
            valid=false
        }
        //Update errors for render
        if (errors.name) {
            setNameError(errors.name ? errors.name : '')
            valid = false
        } else {
            setNameError('')
        }
        if (errors.email) {
            setEmailError(errors.email ? errors.email : '')
            valid = false
        } else {
            setEmailError('')
        }
        if (errors.password) {
            setPasswordError(errors.password ? errors.password : '')
            valid = false
        } else {
            setPasswordError('')
        }
        if (errors.repeatPassword) {
            setRepeatPasswordError(errors.repeatPassword ? errors.repeatPassword : '')
            valid = false
        } else {
            setRepeatPasswordError('')
        }
        //Return validation
        return valid
    }

    const validateEmail = () => {
        let valid = true
        let errors = {}
        //Validation of email
        if (!email)  {
            errors.email = 'Introduce un email'
            valid = false
        }
        //Update errors for render
        if (errors.email) {
            setEmailError(errors.email ? errors.email : '')
            valid = false
        } else {
            setEmailError('')
        }
        //Return validation
        return valid
    }

    const validateName = () => {
        let valid = true
        let errors = {}
        //Validation of name
        if (!name)  {
            errors.name = 'Introduce un nombre'
            valid = false
        }
        //Update errors for render
        if (errors.name) {
            setNameError(errors.name ? errors.name : '')
            valid = false
        } else {
            setNameError('')
        }
        //Return validation
        return valid
    }

    const validatePassword = () => {
        let valid = true
        let errors = {}
        //Validation of password
        if (!password)  {
            errors.password = 'Introduce una contraseña'
            valid = false
        } else if (password.length < 6){
            errors.password = 'La contraseña debe tener al menos 6 caracteres'
            valid = false
        }
        //Update errors for render
        if (errors.password) {
            setPasswordError(errors.password ? errors.password : '')
            valid = false
        } else {
            setPasswordError('')
        }
        //Return validation
        return valid
    }

    const login = () => {
        Actions.replace("SignInScreen")
    }

    const register = () => {
        if (!validateForm()) return null
        setActivity(true)
        auth()
            .createUserWithEmailAndPassword(email, password)
            .then( () => {
                createUser()
            })
            .catch( error => {
                setActivity(false)
                const user = {
                    email: email,
                    name: name, 
                    role: 'user', 
                }
                addUserToFirebase(user)
            })
    }

    const createUser = () => {
        const user = {
            email: email,
            name: name,
            role: 'user',
        }
        setName('')
        setNameError('')
        setEmail('')
        setEmailError('')
        setPassword('')
        setPasswordError('')
        setRepeatPassword('')
        setRepeatPasswordError('')
        setActivity(false)
        updateUserLogged(user)
        addUserToFirebase(user)
    }

    const addUserToFirebase = (user) => {
        const db = firebase.firestore().collection('Users')
        //Ask for user
        db.where("email", "==", user.email)
            .get()
            .then( data => {
                const dataReaded = []
                data.forEach( doc => {
                    dataReaded.push(doc.id)
                })
                //If user exits, update user
                if (dataReaded[0]){
                    console.log('Usuario ya existe')
                    db.doc(dataReaded[0]).update(user)
                        .then( refDoc => {
                            Actions.replace("_Blog", { email })
                            Actions.SPAlert({ 
                                title: `Bienvenido ${user.email}`, 
                                text: 'Has sido añadido a nuestra comunidad',
                                setHeight: hp('4')
                            })
                        })
                        .catch( error => {
                            Actions.SPAlert({ 
                                title: `Error ${user.email}`, 
                                text: 'No puedes realizar esta operación',
                                setHeight: hp('4')
                            })
                            console.log('Error: ', error)
                        })
                } 
                //If user not exits, create user
                else {
                    db.add(user)
                        .then( refDoc => {
                            console.log('Usuario no existe')
                            Actions.replace("_Blog", { email })
                            Actions.SPAlert({ 
                                title: `Bienvenido ${user.email}`, 
                                text: 'Has sido añadido a nuestra comunidad',
                                setHeight: hp('4')
                            }) 
                        })
                        .catch( error => {
                            Actions.SPAlert({ 
                                title: `Error ${user.email}`, 
                                text: 'No puedes realizar esta operación',
                                setHeight: hp('4')
                            })
                            console.log('Error: ', error)
                        })
                }
            })
            .catch( error => {
                Actions.SPAlert({ 
                    title: `Error ${user.email}`, 
                    text: 'No puedes realizar esta operación',
                    setHeight: hp('4')
                })
                console.log('Error: ', error)
            })
    }

    //Renders
    return(
        <View style={ styles.textStyle }>
            <KeyboardAvoidingScrollView
                containerStyle={ styles.formStyle }
            >
                <TouchableOpacity 
                    style={ styles.imageBack }
                    onPress={ () => Actions.replace('WelcomeScreen') }
                >
                    <Back />
                </TouchableOpacity>
                <View style={{ height: hp('5') }} />
                <Image
                    source={ require('../../resources/Logo.png')}
                    resizeMode='contain'
                    style={ styles.image }
                />
                <View style={{ height: Platform.OS === 'ios' ? hp('8.2') : hp('4') }} />
                <Text style={ styles.text }>Registro</Text>
                <View style={{ height: hp('3.68') }} />
                <SapphiraInput
                    label='Nombre completo'
                    placeholder='Nombre completo'
                    value={ name }
                    error={ nameError }
                    onChangeText={ v => setName(v) }
                    keyboardType='default'
                    onSubmitEditing={ validateForm }
                    onChange={ validateName }
                    multiline={ false} 
                    isPassword={ false }
                />
                <View style={{ height: hp('2.8') }} />
                <SapphiraInput
                    label='Correo electrónico'
                    placeholder='Correo electrónico'
                    value={ email }
                    error={ emailError }
                    onChangeText={ v => setEmail(v) }
                    keyboardType='email-address'
                    onSubmitEditing={ validateForm }
                    onChange={ validateEmail }
                    multiline={ false} 
                    isPassword={ false }
                />
                <View style={{ height: hp('2.8') }} />
                <SapphiraInput
                    label='Contraseña'
                    placeholder='Contraseña'
                    value={ password }
                    warning={ warning }
                    error={ passwordError }
                    onChangeText={ v => setPassword(v) }
                    keyboardType='default'
                    onSubmitEditing={ validateForm }
                    onEndEditing={ validatePassword }
                    onChange={ validatePassword }
                    multiline={ false} 
                    isPassword={ true }
                />
                <View style={{ height: hp('2.8') }} />
                <SapphiraInput
                    label='Repetir contraseña'
                    placeholder='Repetir contraseña'
                    value={ repeatPassword }
                    warning={ warning }
                    error={ repeatPasswordError }
                    onChangeText={ v => setRepeatPassword(v) }
                    keyboardType='default'
                    onSubmitEditing={ validateForm }
                    onEndEditing={ validateForm }
                    onChange={ validateForm }
                    multiline={ false} 
                    isPassword={ true }
                />
                <View style={{ height: hp('5.95') }} />
                <TUA_Button
                    bgColor={ '#CBAC77' }
                    label={ 'Registrarme' }
                    labelColor={ '#FFFFFF'}
                    setWidth={ Dimensions.get('window').width - wp('14.96') }
                    onPress={ register }
                    fontSize={ hp('1.7') }
                    setActivity={ activity }
                    setHeight={ hp('5.5') }
                    buttonStyle={{ borderRadius: hp('0.5'), marginLeft: wp('7.46')}}
                />
                <View style={{ height: hp('2.15') }} />
                <View style={ styles.registerView }>
                    <Text style={ styles.textRegisterQuestion }>¿Ya tienes cuenta?</Text>
                    <TouchableOpacity
                        transparent
                        onPress={ login }
                    >
                        <Text style={ styles.textRegister }>   Inicia tu sesion</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingScrollView>
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    textStyle: {
        flex: 1,
        backgroundColor: '#F9F9F9',
    },
    formStyle: {
        flex: 1,
        backgroundColor: '#F9F9F9',
    },
    viewStyle: {
        height: hp('44'),
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.75'),
        marginLeft: Platform.OS === 'ios' ? wp('9.1') : wp('9.1'),
        paddingLeft: Platform.OS === 'ios' ? wp('2.5') : wp('2.5'),
    },
    image: {
        marginTop: hp('4.75'),
        marginLeft: wp('8.2'),
    },
    registerView: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: wp('-6'),
    },
    textRegisterQuestion: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#707070',
    },
    textRegister: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#CBAC77',
        textDecorationLine: 'underline',
    },
    imageBack: {//
        marginLeft: wp('6'),
        marginTop: hp('5'),
        position: 'absolute',
        top: hp('0'),
        left: wp('0'),
    },
})

export default SignUpScreen