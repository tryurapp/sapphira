//React imports
import React, { useState, useEffect } from 'react'
//React native imports
import { View, Text, StyleSheet } from 'react-native'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//Redux imports
import { useDispatch, useSelector } from 'react-redux'
import * as SPActions from '../redux/actions'
import { TouchableOpacity } from 'react-native'
//Images imports
import BackDateHours from '../resources/appointments/backDateHours'
import NextDateHours from '../resources/appointments/nextDateHours'
import moment from 'moment'
//Utils imports
import * as Utils from '../utils'
//Firebase imports
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/firestore'
// Rect native router flux imports
import { Actions } from 'react-native-router-flux'


const TimeRange = () => {

    //Connect Redux for functional component
    const dateSelected = useSelector(
        state => state.appointments.dateSelected
    )
    const pickerTherapist = useSelector(
        state => state.appointments.pickerTherapist
    )
    const slots = useSelector(
        state => state.appointments.slots
    )
    const user = useSelector(
        state => state.login.userLogged
    )
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updateTimeSelected = (timeSelected) => {
        dispatch(SPActions.appointments.updateTimeSelected(timeSelected))
    }
    const updateDateSelected = (dateSelected) => {
        dispatch(SPActions.appointments.updateDateSelected(dateSelected))
    }

    //State
    const [timeSelected, setTimeSelected] = useState('')
    const [stateHours, setStateHours] = useState([])

    //useEffect
    useEffect( () => {
        if (dateSelected) {
            let selectedSlots = []
            const dateToConvertFrom = moment(`${ dateSelected } 00:00 am`, "DD/MM/YYYY hh:mm a").toDate()
            const dateFrom = firebase.firestore.Timestamp.fromDate(dateToConvertFrom)
            const dateToConvertTo = moment(`${ dateSelected } 23:59 pm`, "DD/MM/YYYY hh:mm a").toDate()
            const dateTo = firebase.firestore.Timestamp.fromDate(dateToConvertTo)
            slots.forEach( slot => {
                if (slot.serviceDate > dateFrom && slot.serviceDate < dateTo) {
                    selectedSlots.push(slot)
                }
            });
            setStateHours(selectedSlots)
        }
    }, [dateSelected, slots])

    //Constants
    const HORA_INICIO = 9

    //Functions
    const selectedTime = (time) => {
        setTimeSelected(time)
        updateTimeSelected(time)
    }

    const pastDay = () =>{
        const today = moment().format('DD/MM/YYYY')
        const newToday = moment(today, 'DD/MM/YYYY')
        const newDate = moment(dateSelected, 'DDMMYYYY')
        const subDate = newDate.subtract(1, 'days')
        if (subDate > newToday) {
            const previousDate = subDate.format('DD/MM/YYYY')
            updateDateSelected(previousDate)
        } else {
            Actions.SPAlert({ 
                title: `Atención.`, 
                text: 'Para poder agendar, la fecha debe ser mayor que hoy.',
                setHeight: hp('4')
            })
        }
    }

    const nextDay = () => {
        const maxDate = moment(Date()).add(2, 'months').calendar()
        const newMaxDate = moment(maxDate, 'MM/DD/YYYY').format('DD/MM/YYYY')
        const newMaxFormated = moment(newMaxDate, 'DD/MM/YYYY')
        const newDate = moment(dateSelected, 'DDMMYYYY')
        const subDate = newDate.add(1, 'days')
        if (subDate <= newMaxFormated) {
            const nextDate = subDate.format('DD/MM/YYYY')
            updateDateSelected(nextDate)
        } else {
            Actions.SPAlert({ 
                title: `Atención.`, 
                text: 'No se pueden agendar citas con mas de dos meses de antelación.',
                setHeight: hp('4')
            })
        }
    }

    //Renders
    const setSelectableTime = (time) => {
        const dateSel = moment(dateSelected, 'DD/MM/YYYY d').format('DD/MM/YYYY d').substring(11, 12)
        let state = 'available'
        const timeToSelect = Utils.functions.calculateTime(time)
        if (timeToSelect === timeSelected) {
            state = 'selected'
        }
        if ( dateSel === '0') {
            state = 'notAvailable'
        } else {
            for (let i=0; i<stateHours.length; i++) {
                if (stateHours[i].therapistName === pickerTherapist.name && stateHours[i].serviceHour === time || (stateHours[i].customerEmail === user.email && stateHours[i].serviceHour === time)) {
                    state = 'notAvailable'
                    break
                }
            }
        }
        
        let styleType = {}
        let styleText = {}
        if (state === 'notAvailable') {
            styleType = styles.notAvailable
            styleText = styles.textNotAvailable
        } else if (state === 'selected') {
            styleType = styles.selected
            styleText = styles.textSelected
        } else {
            styleType = styles.selectable
            styleText = styles.textSelectable
        }
        return(
            <View>
                {
                    state === 'available' 
                    ?
                    <TouchableOpacity
                        transparent
                        onPress={ () => selectedTime(timeToSelect) }
                    >
                        <View style={ styleType }>
                            <Text style={ styleText }>{ timeToSelect }</Text>
                        </View>
                    </TouchableOpacity>
                    :
                    <View style={ styleType }>
                        <Text style={ styleText }>{ timeToSelect }</Text>
                    </View>
                }
            </View>
        )
    }

    const renderHeader = () => {
        return(
            <View style={ styles.headerContainer}>
                <View style={{ flexDirection: 'row'}}>
                    <TouchableOpacity
                        transparent
                        onPress={ pastDay }
                    >
                        <BackDateHours />
                    </TouchableOpacity>
                    <View style={{ width: wp('3.52') }}/>
                    <Text style={ styles.headerText }>{ dateSelected }</Text>
                    <View style={{ width: wp('3.52') }}/>
                    <TouchableOpacity
                        transparent
                        onPress={ nextDay }
                    >
                        <NextDateHours />
                    </TouchableOpacity>
                    
                </View>
            </View>
        )
    }

    return(
        <View style={ styles.container }>
            {
                dateSelected
                ?
                <View>
                    <View style={{ height: hp('2.7') }} />
                    {
                        renderHeader()
                    }
                    <View style={{ height: hp('2.7')}} />
                    <View style={ styles.row }>
                        { setSelectableTime(HORA_INICIO) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+1) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+2) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+3) }   
                    </View>
                    <View style={{ height: hp('1.62') }} />
                    <View style={ styles.row }>
                        { setSelectableTime(HORA_INICIO+4) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+5) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+6) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+7) }   
                    </View>
                    <View style={{ height: hp('1.62') }} />
                    <View style={ styles.row }>
                        { setSelectableTime(HORA_INICIO+8) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+9) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+10) } 
                        <View style={{ width: wp('4.64') }} />
                        { setSelectableTime(HORA_INICIO+11) }   
                    </View>
                </View>
                :
                <Text style={ styles.textWarning }>Debe seleccionar una fecha</Text>
            }
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    row: {
        flexDirection: 'row',
        marginLeft: wp('9'),
    },
    selectable: {
        width: wp('17'),
        height: hp('4'),
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#707070',
        borderWidth: wp('0.2'),
        borderRadius: wp('1')
    },
    selected: {
        width: wp('17'),
        height: hp('4'),
        backgroundColor: '#CBAC7739',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#CBAC77',
        borderWidth: wp('0.2'),
        borderRadius: wp('1')
    },
    notAvailable: {
        width: wp('17'),
        height: hp('4'),
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#E2E2E2',
        borderWidth: wp('0.2'),
        borderRadius: wp('1')
    },
    textSelectable: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#707070',
    },
    textSelected: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#222427',
    },
    textNotAvailable: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#E2E2E2',
    },
    textWarning: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#FF3A20',
        marginLeft: wp('10'),
    },
    headerContainer: {
        justifyContent: 'center',
        marginLeft: wp('32.68')
    },
    headerText: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.5'),
        color: '#222427',
        marginTop: hp('0.5'),
    },
})

export default TimeRange