//React imports
import React, { Component } from 'react'
//React native imports
import { TouchableOpacity, Text, Dimensions, View, ActivityIndicator } from 'react-native'
//PropTypes imports
import PropTypes from 'prop-types'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'


export default class AppButton extends Component {

    //PropTypes
    static propTypes = {
        bgColor: PropTypes.string,
        label: PropTypes.string,
        labelColor: PropTypes.string,
        setWidth: PropTypes.number,
        onPress: PropTypes.func,
        buttonStyle: PropTypes.object,
        fontSize: PropTypes.number,
        setActivity: PropTypes.bool,
        setHeight: PropTypes.number,
    }

    //State
    static defaultProps = {
        bgColor: 'white',
        label: 'Button',
        labelColor: 'black',
        onPress: () => {},
        fontSize: (Platform.OS === 'ios') ? hp('2.5') : hp('1.7'),
        setActivity: false,
        borderWidth: 0,
    }

    //Functions
    _onPress() {
        this.props.onPress()
    }

    //Renders
    render() {
        const { bgColor, onPress, label, labelColor, setWidth, buttonStyle, fontSize, setActivity, setHeight, borderWidth } = this.props
        const width = !setWidth ? Dimensions.get('window').width - 60 : setWidth
        const height = !setHeight ? hp('6') : setHeight
        return (
            <TouchableOpacity 
                style={[{
                    backgroundColor: bgColor,
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: height,
                    borderRadius: hp('3'),
                    width: width,
                    borderColor: '#CBAC77',
                    borderWidth: borderWidth,
                }, buttonStyle]} 
                onPress={ onPress }
            >
                <View 
                    style={{ 
                        flexDirection: 'row', justifyContent: 'space-between' }}
                >
                    {
                        setActivity ?
                                <ActivityIndicator 
                                    size="small" 
                                    color='white'
                                />
                            : null
                    } 
                    {
                        setActivity ?
                            <View style={{ width: wp('5') }} />
                            : null
                    } 
                    <Text 
                        style={{
                            color: labelColor,
                            fontSize: fontSize,
                            fontFamily: 'Helvetica Neue',
                            marginLeft: wp('0') }}
                    >
                        { label }
                    </Text>
                </View>            
            </TouchableOpacity>
        )
    }
}