//React imports
import React from 'react'
//React native imports
import { View, Text, StyleSheet, Dimensions } from 'react-native'
//Components imports
import TUA_Button from './TUA_Button'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React native router flux imports
import { Actions } from 'react-native-router-flux'
//Images imports
import LogoAlert from '../resources/LogoAlert'


const SPAlert = props => {

    //Constants
    const { title, text, setHeight, onPress, buttonText, email } = props

    const goOnPress = () => {
        if (onPress === 'GoToStore') {
            Platform.OS === 'android' 
            ? 
            Actions.replace('Store')
            :
            Actions.replace('_Blog', { email })
        } else {
            Actions.pop()
        }
    }

    //Renders
    return(
        <View style={ styles.container }>
            <View style={ styles.box }>
                <View style={ styles.imageView }>
                    <LogoAlert 
                        height={ hp('6') }
                        width={ hp('6') }
                    />
                </View>
                <Text style={ styles.title }>{ title }</Text>
                <Text style={ styles.text }>{ text }</Text>
                <View style={ styles.button }>
                    <TUA_Button
                        bgColor={ '#CBAC77' }
                        label={ buttonText ? buttonText : 'Continuar' }
                        labelColor={ '#FFFFFF' }
                        setWidth={ wp('30.4') }
                        onPress={ goOnPress }
                        fontSize={ hp('1.7') }
                        setActivity={ false }
                        //setHeight={ hp('4.66') }
                        buttonStyle={{ marginLeft: wp('0'), borderRadius: hp('0.55'),}}
                        borderWidth={ wp('0.2') }
                        setHeight={ setHeight }
                    />
                </View>
            </View>
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#e2e2e2CC',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    box: {
        width: wp('78'),
        borderRadius: wp('2.36'),
        backgroundColor: '#F9F9F9',
        paddingLeft: wp('4'),
        paddingRight: wp('4'),
        paddingBottom: hp('3.24'),
    },
    title: {
        fontFamily: 'Helvetica Neue Medium',
        fontSize: hp('1.94'),
        marginTop: hp('4.32'),
    },
    text: {
        marginTop: hp('2.16'),
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.62'),
        lineHeight: hp('2.16'),
    },
    button: {
        alignItems: 'center',
        marginTop: hp('3'),
    },
    imageView: {
        position: 'absolute',
        left: (Dimensions.get('window').width - wp('24')) / 2 - hp('3'),
        top: 0 - hp('3'),
    },
})

export default SPAlert