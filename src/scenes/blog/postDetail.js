//React imports
import React, { useState } from 'react'
//React native imports
import { Text, StyleSheet, View, Image, Dimensions, TouchableOpacity, useWindowDimensions } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React Native Router Flux imports
import { Actions } from 'react-native-router-flux'
//Redux imports
import { useSelector, useDispatch } from 'react-redux'
import * as SPActions from '../../redux/actions'
//Images imports
import Fav_on from '../../resources/blog/favouriteOn'
import Fav_off from '../../resources/blog/favouriteOff'
import Back from '../../resources/blog/back'
import HTML from 'react-native-render-html'
import moment from 'moment'
//Firebase imports
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/firestore'


const PostDetail = props => {

    //Connect Redux for functional component
    const user = useSelector(
        state => state.login.userLogged
    )
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updateUserLogged = (userLogged) => {
        dispatch(SPActions.login.updateUserLogged(userLogged))
    }

    //State
    const [userFav, setUserFav] = useState(user.favourites)

    //Constants
    const { item } = props

    //Functions
    const changeFav = (postId) => {
        if (user.email !== 'invited') {
            let isFav = false
            if (userFav) {
                userFav.forEach( item => {
                    if (item === postId) {
                        isFav = true
                    }
                })
            }
            !isFav ?
                addPostToFavourites(item.id)
            :
                deletePostFromFavourites(item.id)
        } else {
            Actions.SPAlert({ 
                title: 'Inicia sesión', 
                text: 'Para poder Guardar posts debes haber iniciado tu sesión',
                setHeight: hp('4')
            })
        }
    }

    const addPostToFavourites = (postId) => {
        let userPass = []
        if (userFav) {
            userPass = userFav
        }
        userPass.push(postId)
        setUserFav(userPass)
        const userToUpdate = {
            favourites: userPass
        }
        updateUser(userToUpdate, 'add')
    }

    const deletePostFromFavourites = (postId) => {
        let userPass = []
        if (userFav) {
            userPass = userFav
            userPass.forEach( (item, index) => {
                if (item === postId) {
                    userPass.splice(index, 1)
                    setUserFav(userPass)
                }
            })
        }
        const userToUpdate = {
            favourites: userPass
        }
        updateUser(userToUpdate, 'delete')
    }

    const updateUser = (userToUpdate, type) => {
        let newUser = user
        const db = firebase.firestore().collection('Users')
        //Ask for user
        db.where("email", "==", user.email)
        .get()
        .then( data => {
            const dataReaded = []
            data.forEach( doc => {
                dataReaded.push(doc.id)
            })
            if (dataReaded[0]) {
                db.doc(dataReaded[0]).update(userToUpdate)
                .then( refDoc => {
                    newUser.favourites = userToUpdate.favourites
                    updateUserLogged(newUser)
                    Actions.refresh()
                })
            }
        })
        .catch( error => {
            console.log('Error: ', error)
        })
    }

    //Renders
    const renderFav = (postId) => {
        let isFav = false
        if (userFav) {
            userFav.forEach( item => {
                if (item === postId) {
                    isFav = true
                }
            })
        }
        return (
            <View>
                {
                    isFav ? <Fav_on /> : <Fav_off />
                }
            </View>
        )
    }

    const renderHTML = (data) => {
        const { width } =useWindowDimensions()
        const source = { html: data.content.rendered }
        return(
            <View style={ styles.captionStyle }>
                <HTML 
                    source={ source } 
                    contentWidth={ width }
                />
            </View>
        )
    }
    
    return(
        <View style={ styles.container }>
            {
                item._embedded['wp:featuredmedia'].filter( element => element.id == item.featured_media).map( (subitem, index) => (
                    <Image
                        source={{ uri: subitem.media_details.sizes.medium.source_url }}
                        style={ styles.imagePost }
                        key={ item.id }
                    />
                ))
            }
            <TouchableOpacity 
                style={ styles.imageBack }
                onPress={ () => Actions.replace('_Blog') }
            >
                <Back />
            </TouchableOpacity>
            <ScrollView  style={ styles.textCaption }>
                <Text style={ styles.subtitle }>{ moment(item.date).format("ll") }</Text>
                <Text style={ styles.postTitle }>{ item.title.rendered }</Text>
                {
                    renderHTML(item)    
                }
            </ScrollView>
            <TouchableOpacity 
                style={ styles.imageFav }
                onPress={ () => changeFav(item.id) }
            >
                {
                    renderFav(item.id)
                }
            </TouchableOpacity>
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9',
    },
    imageBack: {
        marginLeft: wp('6'),
        marginTop: hp('5'),
        position: 'absolute',
        top: hp('0'),
        left: wp('0'),
    },
    imageFav: {
        position: 'absolute',
        top: hp('28'),
        right: wp('4.64'),
    },
    imagePost: {
        position: 'absolute',
        height: hp('34'),
        width: Dimensions.get('window').width,
        top: 0,
        left: 0,
        justifyContent: 'center',
    },
    textCaption: {
        position: 'absolute',
        top: hp('30'),
        left: 0,
        height: hp('68'),
        width: Dimensions.get('window').width,
        backgroundColor: '#F9F9F9',
        borderTopLeftRadius: wp('5.84'),
        borderTopRightRadius: wp('5.84'),
        paddingTop: wp('3'),
    },
    subtitle: {
        color: '#707070',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.3'),
        marginLeft: wp('8.2'),
        marginTop: hp('4.66'),
    },
    postTitle: {
        fontFamily: 'Helvetica Neue Medium',
        fontSize: hp('2.7'),
        marginTop: hp('1.5'),
        marginLeft: wp('8.2'),
        marginRight: wp('8.2'),
        color: '#222427'
    },
    captionStyle: {
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.74'),
        marginTop: hp('4.32'),
        marginLeft: wp('8.2'),
        marginRight: wp('8.2'),
        lineHeight: hp('2.3'),
        marginBottom: hp('2.3'),
    },
})

export default PostDetail