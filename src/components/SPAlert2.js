//React imports
import React from 'react'
//React native imports
import { View, Text, StyleSheet, Dimensions } from 'react-native'
//Components imports
import TUA_Button from './TUA_Button'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React native router flux imports
import { Actions } from 'react-native-router-flux'
//Images imports
import LogoAlert from '../resources/LogoAlert'



const SPAlert2 = props => {

    //Constants
    const { title, text, setHeight, onPress1, onPress2, labelButton1, labelButton2, backColor } = props

    //Renders
    return(
        <View style={ styles.container }>
            <View style={ styles.box }>
                <View style={ styles.imageView }>
                    <LogoAlert 
                        height={ hp('6') }
                        width={ hp('6') }
                    />
                </View>
                <Text style={ styles.title }>{ title }</Text>
                <Text style={ styles.text }>{ text }</Text>
                <View style={ styles.button }>
                    <TUA_Button
                        onPress={ () => {
                            onPress1()
                            Actions.pop() 
                        }}
                        label={ labelButton1 }
                        bgColor={ '#CBAC77' }
                        labelColor={ '#FFFFFF' }
                        setWidth={ wp('30.4') }
                        fontSize={ hp('1.7') }
                        setActivity={ false }
                        buttonStyle={{ marginLeft: wp('0'), borderRadius: hp('0.55'),}}
                        borderWidth={ wp('0.2') }
                        setHeight={ setHeight }
                    />
                    <View style={{ width: wp('5') }} />
                    <TUA_Button
                        onPress={ () => {
                            onPress2()
                            Actions.pop() 
                        }}
                        label={ labelButton2 }
                        bgColor={ '#CBAC77' }
                        labelColor={ '#FFFFFF' }
                        setWidth={ wp('30.4') }
                        fontSize={ hp('1.7') }
                        setActivity={ false }
                        buttonStyle={{ marginLeft: wp('0'), borderRadius: hp('0.55'),}}
                        borderWidth={ wp('0.2') }
                        setHeight={ setHeight }
                    />
                </View>
            </View>
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e2e2e2CC',
    },
    box: {
        width: Dimensions.get('window').width - wp('20'),
        borderRadius: wp('4'),
        backgroundColor: '#FFFFFF',
        padding: wp('4'),
    },
    title: {
        fontFamily: 'Helvetica Neue Bold',
        fontSize: hp('2'),
        marginTop: hp('3'),
    },
    text: {
        marginTop: hp('2'),
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.65'),
        lineHeight: hp('2.1'),
    },
    button: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: hp('3'),
    },
    imageView: {
        position: 'absolute',
        left: (Dimensions.get('window').width - wp('24')) / 2 - hp('3'),
        top: 0 - hp('3'),
    },
    backgroundLogo: {
        height: hp('7'),
        width: hp('7'),
        borderRadius: hp('4'),
        backgroundColor: '#929291E0',
        position: 'absolute',
        left: (Dimensions.get('window').width - wp('24')) / 2 - hp('3.5'),
        top: 0 - hp('3.5'),
    },
})

export default SPAlert2