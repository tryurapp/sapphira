//React imports
import React, { Component } from 'react'
//React native imports
import { View, StyleSheet } from 'react-native'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//Images imports
import TabBarBlogOn from '../resources/tabBar/tabBarBlogOn'
import TabBarBlogOff from '../resources/tabBar/tabBarBlogOff'



export default class TabIconBlog extends Component {

    //Renders
    render() {
        let imageIcon = true
        if (this.props.focused) {
            imageIcon = true
        } else {
            imageIcon = false
        }
        
        return(
            <View 
                style={ styles.view }>
                {
                    imageIcon ?
                        <TabBarBlogOn
                            width = { wp('12') }
                            height = { hp('20') }
                        />
                    :
                        <TabBarBlogOff
                            width = { wp('12') }
                            height = { hp('20') }
                        />
                }
            </View>
        )
    }
}

//Styles
const styles = StyleSheet.create({
    view:{
        flex: 1, 
        flexDirection: 'column', 
        alignItems: 'center', 
        justifyContent: 'center', 
        alignSelf: 'center',
        marginTop: wp('2'),
    },
});