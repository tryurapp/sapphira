//React imports
import React, { useState, useEffect } from 'react'
//React native imports
import { View, Text, StyleSheet, Image } from 'react-native'
//Components imports
import TUA_Button from './TUA_Button'
import SapphiraInput from './SapphiraInput'
//Responsive screen imports
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
//React native router flux imports
import { Actions } from 'react-native-router-flux'
//Redux imports
import { useSelector, useDispatch } from 'react-redux'
import * as SPActions from '../redux/actions'
//Firebase imports
import '@react-native-firebase/firestore'
//Keyboard imports
import { KeyboardAvoidingScrollView } from 'react-native-keyboard-avoiding-scroll-view'
//Images import
import AvatarDefault from '../resources/profile/avatarDefault'



const UserForm = props => {

    //Constants
    const { user, avatarLoaded } = props

    //Connect Redux for functional component
    const pickerCenter = useSelector(
        state => state.appointments.pickerCenter
    )
    const centersData = useSelector(
        state => state.blog.centers
    )
    //equivalent to mapDispatchToProps
    const dispatch = useDispatch()
    const updatePickerCenter = (pickerCenter) => {
        dispatch(SPActions.appointments.updatePickerCenter(pickerCenter))
    }
    const updateUserLogged = (userLogged) => {
        dispatch(SPActions.login.updateUserLogged(userLogged))
    }
    const updateUserLoggedData = (user) => {
        dispatch(SPActions.login.updateUserLoggedData(user))
    }

    //State
    const [name, setName] = useState(user.name ? user.name : '')
    const [phone, setPhone] = useState(user.phone ? user.phone : '')
    const [email, setEmail] = useState(user.email ? user.email : '')

    useEffect( () => {
        if (user.preferredCenter) {
            updatePickerCenter({
                'name': user.preferredCenter,
                'id': ''
            })
        }
    }, [])

    //Functions
    const acept = () => {
        let centerSelected = {}
        if (pickerCenter.name) {
            centersData.forEach( item => {
                if (item.center.centerName === pickerCenter.name) {
                    centerSelected = item.center
                }
            })
            updatePickerCenter('')
        }
        const user = {
            name: name,
            email: email,
            phone: phone,
            preferredCenter: centerSelected.centerName ? centerSelected.centerName : '',
            phoneCenter: centerSelected.telephoneNumber ? centerSelected.telephoneNumber : '',
            addressCenter: centerSelected.address ? centerSelected.address : ''
        }
        updateUserLogged(user)
        updateUserLoggedData(user)
        Actions.pop( { refresh: {} } )
    }

    const cancel = () => Actions.pop( { refresh: {} } )

    const selectCenter = () => {
        if (!centersData[0]) return noCenter()
        const centers = [{ 'name': 'Seleccione centro' }]
        centersData.forEach( item => {
            const data = { 
                'name': item.center.centerName,
                'id': item.center.centerId 
            }
            centers.push(data)
        })
        Actions.CenterPicker({ centers })
    }

    const noCenter = () => {
        Actions.SPAlert({ 
            title: 'Aviso', 
            text: 'No hay ningún centro creado.',
            setHeight: hp('4')
        })
    }

    const validateForm = () => true

    const validateFormCenter = () => true

    if (!pickerCenter) {
        const center = {
            'name': user.preferredCenter,
            'id': ''
        }
        updatePickerCenter(center)
    }

    //Renders
    return(
        <View style={ styles.container }>
            <View style={ styles.avatarView }>
                {
                    avatarLoaded
                    ?
                    <Image 
                        large 
                        source={{ uri: avatarLoaded }}
                        style={ styles.thumbnail }
                    />
                    :
                    <View style={ styles.thumbnail }>
                        <AvatarDefault 
                            width={ hp('12') }
                            height={ hp('12') }
                            borderRadius={ hp('6') }
                        />
                    </View>
                    }
                <View style={{ width: wp('5') }} />
                <View style={{ marginTop: hp('5')}}>
                    <Text style={ styles.textName }>{ user.name }</Text>
                    <Text style={ styles.textEmail }>{ user.email }</Text>
                </View>
            </View>
            <View style={{ height: hp('8') }} />
            <Text style={ styles.text }>Actualizar datos</Text>
            <View style={{ height: hp('3') }} />
            <KeyboardAvoidingScrollView>
                <SapphiraInput 
                    label='Centro de preferencia'
                    placeholder='Centro de preferencia'
                    value={ pickerCenter.name }
                    onChangeText={ () => {} }
                    onFocus={ selectCenter }
                    keyboardType='default'
                    onSubmitEditing={ validateFormCenter }
                    onEndEditing={ validateFormCenter }
                    multiline={ false} 
                    isPassword={ false }
                    editable={ false }
                />
                <View style={{ height: hp('2.5') }} />
                <SapphiraInput
                    label='Nombre'
                    placeholder='Nombre'
                    value={ name }
                    onChangeText={ v => setName(v) }
                    keyboardType='default'
                    onSubmitEditing={ validateForm }
                    multiline={ false} 
                    isPassword={ false }
                />
                <View style={{ height: hp('2.5') }} />
                <SapphiraInput
                    label='Teléfono de contacto'
                    placeholder='Teléfono de contacto'
                    value={ phone }
                    onChangeText={ v => setPhone(v) }
                    keyboardType='default'
                    onSubmitEditing={ validateForm }
                    multiline={ false} 
                    isPassword={ false }
                />
                <View style={{ height: hp('6') }} />
                <TUA_Button
                    bgColor={ '#CBAC77' }
                    label={ 'Aceptar' }
                    labelColor={ '#FFFFFF' }
                    setWidth={ wp('80') }
                    onPress={ acept }
                    fontSize={ hp('1.7') }
                    setActivity={ false }
                    setHeight={ hp('5.5') }
                    buttonStyle={{ marginLeft: wp('10'), borderRadius: hp('0.5'),}}
                />
                <View style={{ height: hp('2') }} />
                <TUA_Button 
                    bgColor={ '#F9F9F9' }
                    label={ 'Cancelar' }
                    labelColor={ '#CBAC77' }
                    setWidth={ wp('80') }
                    onPress={ cancel }
                    fontSize={ hp('1.7') }
                    setActivity={ false }
                    setHeight={ hp('5.5') }
                    buttonStyle={{ marginLeft: wp('0'), borderRadius: hp('0.5'), marginLeft: wp('10') }}
                />
            </KeyboardAvoidingScrollView>
        </View>
    )
}

//Styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        //marginTop: hp('34'),
        paddingTop: hp('5'),
    },
    text: {
        fontFamily: 'Mr Lackboughs',
        fontSize: hp('4.5'),
        paddingLeft: wp('2.5'),
        marginLeft: wp('10'),
    },
    textExit: {
        color: '#CBAC77',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('2'),
        marginLeft: wp('10'),
        marginTop: hp('7'),
    },
    avatarView: {
        flexDirection: 'row',
        marginTop: hp('6'),
        marginLeft: wp('10'),
        width: wp('50'),
    },
    textName: {
        color: '#222427',
        fontFamily: 'Helvetica Neue Bold',
        fontSize: hp('1.8'),
        marginTop: hp('1'),
    },
    textEmail: {
        color: '#707070',
        fontFamily: 'Helvetica Neue',
        fontSize: hp('1.6'),
        marginTop: hp('1'),
    },
    thumbnail: {
        width: hp('14'),
        height: hp('14'),
        borderRadius: hp('7'),
        resizeMode: 'cover',
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
    },
})

export default UserForm